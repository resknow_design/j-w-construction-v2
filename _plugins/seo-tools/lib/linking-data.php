<?php

/**
 * JSON Linking Data
 *
 * Returns JSON object with local data.
 * @see http://json-ld.org/
 */
function seo_linking_data() {

    # Get Local Data
    $data = (get('site.linking_data') ? get('site.linking_data') : array());

    # Filter: seo_linking_data
    $data = apply_filters('seo_linking_data', $data);

    # If no data, return false.
    if ( !is_array($data) || empty($data) )
        return false;

    # Create Output
    $output = '<!-- Linking Data -->
    <script type="application/ld+json">'. stripslashes(json_encode($data)) .'</script>
    ';

    # Filter: seo_linking_data_output
    $output = apply_filters('seo_linking_data_output', $output);

    # Print data to the <head>
    echo $output;

}
