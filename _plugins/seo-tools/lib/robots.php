<?php

/**
 * Robots.txt
 *
 * @NOTE This will handle requests for
 * robots.txt only if an actual robots.txt
 * does not exist in the root of your site.
 *
 * @param $load (string) (optional) Load from file
 * @return Plain text output
 */
function seo_robots( $load = false ) {
    if ( get('page.path') == 'robots.txt' ) {

        # Get Robots file
        if ( $load !== false && is_readable($load) ) {
            $data = file_get_contents($load);
        } else {
            $data = 'Sitemap: '. get('site.url') .'/sitemap.xml
User-agent: *
Disallow: /_controller
Disallow: /_includes
            ';
        }

        # Filter: seo_robots
        $data = apply_filters('seo_robots', $data);

        # Get Robots settings
        header('Content-type: text/plain');
        echo $data;
        exit();

    }
}
