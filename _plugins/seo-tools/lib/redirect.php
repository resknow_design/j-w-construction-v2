<?php

/**
 * Redirect
 *
 * Redirect a URL to a different location
 *
 * @NOTE This is for 301 Redirects and
 * should not be used for redirecting
 * unauthorised users as there is no
 * exit() call if the redirect fails.
 */
function seo_redirect() {

    # Get Redirects
    $redirects = (get('site.redirects') ? get('site.redirects') : array());

    # Filter: seo_redirects
    $redirects = apply_filters('seo_redirects', $redirects);

    # Check list is not empty
    if ( !is_array($redirects) || empty($redirects) )
        return false;

    # Get current path
    $path = get('page.path');

    # If this path has a redirect assigned, execute it.
    if ( array_key_exists($path, $redirects) ) {
        header('HTTP/1.1 301 Moved Permanently');
        header('location: ' . $redirects[$path]);
    }

}
