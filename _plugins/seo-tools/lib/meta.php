<?php

/**
 * SEO Meta Tags
 *
 * Add additional meta tags to the <head>
 * of each page
 *
 * @return HTML output
 */
function seo_meta_tags() {

    $tags = '';

    # For dev mode, add comment
    if ( get('site.environment') == 'dev' ) {
        $tags .= '<!-- SEO Tags -->' . PHP_EOL;
    }

    $tags .= sprintf('    <meta name="author" content="%s (%s)">', get('site.company'), str_replace('http://', '', get('site.url'))) . PHP_EOL;
    $tags .= '    <meta name="revisit-after" content="15 days">' . PHP_EOL;

    echo $tags;

}
