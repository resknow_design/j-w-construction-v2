<?php

/**
 * @subpackage SEO Tools
 * @package Boilerplate
 * @version 1.0.0
 *
 * This plugin provides some useful
 * tools for adding some SEO techniques
 * to sites built with Boilerplate
 *
 * @NOTE This plugin is *NOT* compatible with
 * Twig templating at the moment. You can however
 * use PHP's get_defined_functions() to assign functions
 * to an object or array for use inside Twig.
 * @see http://stackoverflow.com/a/28232213
 *
 * Setup
 * ===========================
 *
 * 1. In your header.php file, add <?php seo_head(); ?> somewhere
 * in the <head> section.
 *
 * 2. In your footer.php file, add <?php seo_footer(); ?> before
 * the closing </body>.
 *
 * ===========================
 *
 */

# Set minimum version
plugin_requires_version('seo-tools', '1.5.0');

# Include Functions
require_once __DIR__ . '/lib/linking-data.php';
require_once __DIR__ . '/lib/redirect.php';
require_once __DIR__ . '/lib/robots.php';
require_once __DIR__ . '/lib/meta.php';
require_once __DIR__ . '/lib/sitemap.php';

/**
 * Execute Redirects
 *
 * @NOTE Be very careful using redirects as
 * they will be executed no matter which
 * plugin is controlling the output, which can
 * lead to undesired side effects if you're not
 * careful! If you're not sure about what you need
 * to setup, ask.
 *
 * @see seo_redirect() in lib/redirect.php
 */
seo_redirect();

/**
 * Catch request for robots.txt
 * @see seo_robots() in lib/robots.php
 */
seo_robots();

/**
 * Catch request for sitemap.xml
 * @see seo_sitemap() in lib/sitemap.php
 */
seo_sitemap();

/**
 * Head
 *
 * Inject data in to the head of your document
 * @return meta tags and data to be inserted in to <head>
 */
function seo_head() {

    # Trigger: seo_head_before
    do_trigger('seo_head_before');

    # Meta Tags
    seo_meta_tags();

    # JSON Local Data
    seo_linking_data();

    # Trigger: seo_head_after
    do_trigger('seo_head_after');

}

/**
 * Footer
 *
 * Inject data in to the head of your document
 * @return Data to be inserted before </body>
 */
function seo_footer() {

    # Trigger: seo_footer_before
    do_trigger('seo_footer_before');

    # Trigger: seo_footer_after
    do_trigger('seo_footer_after');

}
