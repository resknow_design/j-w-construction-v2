<?php

########
/**
 * Set timezone
 * For Windows dev mode only
 */
date_default_timezone_set('Europe/London');

/**
 * CMS User Logged In
 *
 * Check if the current user is
 * authenticated with the CMS
 * to make edits.
 *
 * @return (bool) True is user is logged in
 */
function cms_user_logged_in() {
    return array_key_exists('cms_user', $_SESSION);
}

/**
 * CMS Get User
 *
 * Get the current user object
 * from the action session
 *
 * @return (Mixed) User object is
 * the user is logged in, false otherwise.
 */
function cms_get_user() {

    if ( !cms_user_logged_in() )
        return false;

    return $_SESSION['cms_user'];

}
