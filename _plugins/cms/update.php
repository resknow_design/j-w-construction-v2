<?php

/**
 * CMS Update
 *
 * This script is designed to be run
 * as a CRON job and should ideally
 * be run during quiet times in case
 * of any errors.
 */

/**
 * Check ZipArchive class is available
 * or exit.
 */
if ( !class_exists('ZipArchive') ) {
    exit('Boilerplate CMS Update requires PHP\'s ZipArchive class.');
}

/**
 * Update Is Available
 *
 * Compare the version of this installation
 * to the current version available on the
 * update server
 */
function update_is_available() {
    $this_version = trim(file_get_contents('version.txt'));
    $current_version = trim(file_get_contents('http://update.resknow.net/current.txt'));
    return $this_version < $current_version;
}

/**
 * Echo Status
 *
 * Echo the status of the current
 * action to the screen
 */
function echo_status($msg) {
    echo '<p>' . $msg . '</p>';
}

/**
 * Get Update Package
 *
 * Download the latest update package
 * from the server
 */
function get_update_package() {
    $version = trim(file_get_contents('http://update.resknow.net/current.txt'));
    echo_status('Current version is: ' . $version);

    $package = file_get_contents('http://update.resknow.net/cms-' . $version . '.zip');
    echo_status('Starting download: http://update.resknow.net/cms-' . $version . '.zip');

    if ( !$package ) {
        echo_status('Update package not found');
        return false;
    }

    echo_status('Downloading update package...');

    if ( file_put_contents('update.zip', $package) ) {
        echo_status('Update downloaded...');
        return true;
    } else {
        echo_status('Update download failed.');
        return false;
    }
}

/**
 * Install Update Package
 *
 * This function assumes the update has
 * already been downloaded.
 */
function install_update_package() {

    if ( !is_readable('update.zip') ) {
        echo_status('Update package does not exist or is not readable. <em>Stopping update process.</em>');
        return false;
    }

    $package = new ZipArchive;

    if ( $package->open('update.zip') === true ) {
        $package->extractTo(__DIR__);
        $package->close();
        echo_status('Update unzipped and installed.');
        return true;
    } else {
        echo_status('Unable to unzip and install package.');
        return false;
    }

}

/**
 * Remove Update Package
 *
 * Delete the package once we're done!
 */
function remove_update_package() {
    if ( file_exists('update.zip') && is_writeable('update.zip') ) {
        if ( unlink('update.zip') ) {
            echo_status('Update package cleaned up.');
            return true;
        }
    } else {
        echo_status('Update package does not exist or is not writeable');
        return false;
    }
}

/**
 * Show User Actions
 *
 * Present the user with some buttons
 */
function show_user_actions() {
    echo_status('<a class="action" href="/admin">Dashboard</a>');
}

?>

<!doctype html>
<html>
<head>
    <title>CMS Update</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Merriweather:400,700">
    <style>
    body {
        background-color: #efefef;
        color: #666;
        font-family: sans-serif;
        font-size: 14px;
        line-height: 1.5;
        margin: 0;
    }
    h1,h2,h3 {
        font-family: Merriweather, sans-serif;
        font-weight: 700;
    }
    .container {
        margin: 80px auto;
        max-width: 420px;
        background-color: white;
        box-shadow: 0 0 24px rgba(0,0,0,.1);
        border-radius: 4px;
        padding: 34px;
    }
    .container p {
        margin: 0 0 3px 0;
        padding: 0;
    }
    a.action {
        display: inline-block;
        border-radius: 3px;
        background-color: #465983;
        color: white;
        padding: 8px 12px;
        text-decoration: none;
        margin-top: 12px;
        font-weight: 600;
    }
    </style>
</head>
<body>

    <div class="container">
        <h2>Update Boilerplate CMS</h2>
        <?php

        #######################
        ### Run update process...
        #######################

        # Check for update
        if ( update_is_available() ) {

            # Get update package
            if ( get_update_package() ) {

                # Install package
                if ( install_update_package() ) {

                    # Clean up
                    remove_update_package();

                }

            }

        } else {

        ?>
        You're all up to date!
        <?php

        }

        # Show User Actions
        show_user_actions();

        ?>
    </div>

</body>
</html>
