<?php

use CMS\CMS;
use CMS\User;

// Check for POST and permissions
if ( !user_can('add_user') || !is_form_data() ) {
    JSON::parse( 100, 'negative', 'Access denied.', null, true );
}

// Get GUMP
$gump = new GUMP();

// Sanitize the data
$data = $gump->sanitize( form_data() );

// Set Validation Rules
$gump->validation_rules(array(
    'username'      => 'required',
    'password'      => 'required|min_len,8',
    'email'         => 'required|valid_email',
    'role'          => 'numeric',
    'status'        => 'numeric'
));

// Set Filter Rules
$gump->filter_rules(array(
    'username'      => 'trim|sanitize_string',
    'password'      => 'trim',
    'email'         => 'trim|sanitize_email',
    'role'          => 'trim|sanitize_numbers',
    'status'        => 'trim|sanitize_numbers'
));

// Run GUMP
if ( !$valid = $gump->run($data) ) {
    JSON::parse( 100, 'negative', $gump->get_readable_errors(true), null, true );
} else {

    // Create new User Object
    $new = new User();

    // Set Props
    $new->username = $valid['username'];
    $new->password = $valid['password'];
    $new->email = $valid['email'];

    // Set Role
    if ( array_key_exists( 'role', $valid ) ) {
        $new->role = $valid['role'];
    }

    // Set Status
    if ( array_key_exists( 'status', $valid ) ) {
        $new->role = $valid['status'];
    }

    // Save the new user
    if ( $new->save() ) {
        JSON::parse( 200, 'positive', 'New user created', null, true );
    }

    // Handle failure
    JSON::parse( 100, 'negative', 'Something went wrong, new user was not created.', $valid, true );

}
