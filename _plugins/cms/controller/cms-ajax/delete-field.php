<?php

use CMS\Field;

/**
 * Delete Field
 *
 * Deletes a field from the database by ID
 */

switch (true) {

    # No data
    case !is_form_data():
        JSON::parse( 100, 'negative', 'No form data', null, true );
        break;

    # Not logged in
    case !cms_user_logged_in():
        JSON::parse( 100, 'negative', 'Permission denied', null, true );
        break;

}

# New GUMP Object
$gump = new GUMP();

#Sanitize input
$data = $gump->sanitize(form_data());

# Get Field ID
$id = $data['id'];

# Load field
$field = new Field($id);

# Check field loaded
if ( $field->id > 0 ) {

    # Delete
    $field->delete();
    $field->save();

    # Done
    JSON::parse( 200, 'positive', 'Field removed', null, true );

} else {
    JSON::parse( 100, 'negative', 'Field not found', null, true );
}
