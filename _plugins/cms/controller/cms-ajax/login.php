<?php

use CMS\CMS;

$cms = CMS::run();
$user = $cms->user();

switch (true) {

    # Already logged in
    case cms_user_logged_in():
        JSON::parse( 100, 'negative', 'User already logged in.', null, true );
        break;

    # No data
    case !is_form_data():
        JSON::parse( 100, 'negative', 'No input submitted', null, true );
        break;

}

# Create GUMP object
$gump = new GUMP();

# Get Data
$data = $gump->sanitize(form_data());

try {

    if ( $user->login($data['username'], $data['password']) ) {

        # Remove logout message if it was never dismissed
        remove_message('user-logout');

        # Trigger: cms_user_login
        do_trigger('cms_user_login', $user);

        JSON::parse( 200, 'positive', 'Logging you in, please wait...', null, true );
    }

    JSON::parse( 100, 'negative', 'Wrong username or password, please try again.', null, true );

} catch ( Exception $e ) {
    JSON::parse( 100, 'negative', $e->getMessage(), array('exception' => $e), true );
}
