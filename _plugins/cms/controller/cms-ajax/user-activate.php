<?php

use CMS\CMS;

# Get User
$cms = CMS::run();
$user = $cms->user();

# Update the user status
$user->activate();

# Save
if ( !$user->save() ) {
    JSON::parse( 100, 'negative', 'A problem occured, status was not changed.', array($user), true );
}

# Success!
JSON::parse( 200, 'positive', 'Status changed', array($user), true );
