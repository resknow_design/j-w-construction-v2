<?php

use \Upload\Storage\Filesystem;
use \Upload\File;
use \Upload\Validation\Mimetype;
use \Upload\Validation\Size;

/**
 * Media Upload Handler
 *
 * @NOTE Currently only images can be
 * uploaded as media objects in BPCMS.
 */

# Check we have POST data
if ( !is_form_data() ) {
    JSON::parse( 100, 'negative', 'No data', form_data(), true );
}

# Create a GUMP object
$gump = new GUMP();

# Sanitize Input
$data = $gump->sanitize(form_data());

# Get file array
if ( !isset($data['field_key']) || empty($data['field_key']) ) {
    JSON::parse( 100, 'negative', 'No field key found', form_data(), true );
}

# Create File Upload Objects
try {

    $media_dir = get('cms.config.media_dir');

    $upload_dir = ROOT_DIR . '/' . $media_dir;

    $storage = new Filesystem($upload_dir);
    $file = new File($data['field_key'], $storage);

} catch (Exception $e) {
    JSON::parse( 100, 'negative', $e->getMessage(), form_data(), true );
}

# Create Unique Filename
$file->setName(uniqid() . '-' . $file->getName());

# Validate
$file->addValidations(array(
    new Mimetype(array(
        'image/png',
        'image/jpeg',
        'image/jpg',
        'image/gif'
    )),
    new Size('2M')
));

# Create Path
$path = '/' . $media_dir . '/' . $file->getNameWithExtension();

# Get image info
$image = array(
    'location'   => $path,
    'name'       => $file->getNameWithExtension(),
    'extension'  => $file->getExtension(),
    'mime'       => $file->getMimetype(),
    'size'       => $file->getSize(),
    'md5'        => $file->getMd5(),
    'dimensions' => $file->getDimensions()
);

# Attempt upload
try {

    $file->upload();

    # Filter: cms_upload_image
    $image = apply_filters( 'cms_upload_image', $image );

    JSON::parse( 200, 'positive', 'Image saved!', $image, true );

} catch ( Exception $e ) {
    JSON::parse( 100, 'negative', 'An error occured while uploading your file', array(
        'errors' => $file->getErrors()
    ), true );
}
