<?php

use CMS\Droplet;
use CMS\Field;
use CMS\Util;

# Check we have POST data
if ( !is_form_data() ) {
    JSON::parse( 100, 'negative', 'No data', null, true );
}

# Types
$types = get('cms.types');

# Create a GUMP object
$gump = new GUMP();

# Sanitize Input
$data = form_data();

# Get Type
$type = $data['droplet_type'];
unset($data['droplet_type']);

# Get Fields for Type
if ( array_key_exists($type, $types) ) {
    $fields = $types[$type]['fields'];
} else {
    JSON::parse( 100, 'negative', 'Invalid type', null, true );
}

# Pass data through GUMP for filter and validate
foreach ( $fields as $key => $field ) {

    # Title is always required for a Droplet
    if ( $key == 'title' ) {
        $validation[$key] = 'required';
    }

    # Validation Rules
    if ( array_key_exists('validate', $field) ) {
        $validation[$key] = $field['validate'];
    }

    # Filter Rules
    if ( array_key_exists('filter', $field) ) {
        $filter[$key] = $field['filter'];
    }

}

# Set Validation Rules
if ( isset($validation) && is_array($validation) ) {
    $gump->validation_rules($validation);
}

# Set Filter Rules
if ( isset($filter) && is_array($filter) ) {
    $gump->filter_rules($filter);
}

# Run GUMP
$clean = $gump->run($data);

# Pass errors back to user
if ( $clean === false ) {
    JSON::parse( 100, 'warning', $gump->get_readable_errors(true), null, true );
}

# Create a new Droplet
$droplet = new Droplet();

# Set Title
$droplet->title = $clean['title'];

# Make a slug with the title
$droplet->slug = Util::slug($clean['title']);

unset($clean['title']);

# Set Droplet info
$droplet->type = $type;
$droplet->template = $type;

# Save the Droplet
if ( !$droplet->save() ) {
    JSON::parse( 100, 'negative', 'An error occured. Droplet was not saved', null, true );
}

# Trigger: cms_add
do_trigger('cms_add', $droplet);

# Get Droplet ID
$id = $droplet->id;

# Loop through and add fields to the Droplet
foreach ( $clean as $key => $val ) {

    # If this is not a valid field for this type, skip it.
    if ( !array_key_exists($key, $fields) ) {
        continue;
    }

    # Create a new Field object
    $field = new Field();
    $field->droplet = $id;
    $field->slug = $key;
    $field->type = $fields[$key]['type'];
    $field->value = $val;

    if ( !$field->save() ) {
        JSON::parse( 100, 'negative', 'An error occured. Field was not saved', array('field' => $field), true );
    }

    # Trigger: cms_add_field
    do_trigger('cms_add_field', $field);

}

# Save the Droplet again if it changed
if ( $droplet->has_changed() ) {
    $droplet->save();
}

# Trigger: cms_save_new
do_trigger('cms_save_new', $droplet);

# All done!
JSON::parse( 200, 'positive', 'Saved!', array(
    'id' => $droplet->id
), true );
