<?php

# Get Message ID
if ( !isset($_GET['id']) ) {
    JSON::parse( 100, 'negative', 'No message ID found', null, true );
}

remove_message($_GET['id']);

JSON::parse( 200, 'positive', 'Message dismissed', null, true );
