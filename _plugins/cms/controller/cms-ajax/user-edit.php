<?php

use CMS\CMS;
use CMS\User;

// Check for POST and permissions
if ( !user_can('edit_user') || !is_form_data() ) {
    JSON::parse( 100, 'negative', 'Access denied.', null, true );
}

// Get GUMP
$gump = new GUMP();

// Sanitize the data
$data = $gump->sanitize( form_data() );

// Remove empty password field
if ( empty($data['password']) ) {
    unset($data['password']);
}

// Set Validation Rules
$gump->validation_rules(array(
    'id'            => 'required|numeric',
    'username'      => 'required',
    'password'      => 'min_len,8',
    'email'         => 'required|valid_email',
    'role'          => 'numeric',
    'status'        => 'numeric'
));

// Set Filter Rules
$gump->filter_rules(array(
    'id'            => 'trim|sanitize_numbers',
    'username'      => 'trim|sanitize_string',
    'password'      => 'trim',
    'email'         => 'trim|sanitize_email',
    'role'          => 'trim|sanitize_numbers',
    'status'        => 'trim|sanitize_numbers'
));

// Run GUMP
if ( !$valid = $gump->run($data) ) {
    JSON::parse( 100, 'negative', $gump->get_readable_errors(true), null, true );
} else {

    // Create new User Object
    $edit = new User($valid['id']);

    // Set Props
    $edit->username = $valid['username'];
    $edit->email = $valid['email'];

    // Set Password
    if ( array_key_exists( 'password', $valid ) ) {
        $edit->password = $valid['password'];
    }

    // Set Role
    if ( array_key_exists( 'role', $valid ) ) {
        $edit->role = $valid['role'];
    }

    // Set Status
    if ( array_key_exists( 'status', $valid ) ) {
        $edit->role = $valid['status'];
    }

    // Save the new user
    if ( $edit->save() ) {
        JSON::parse( 200, 'positive', $edit->username . ' updated.', null, true );
    }

    // Handle failure
    JSON::parse( 100, 'negative', 'Something went wrong, your changed were not saved.', $valid, true );

}
