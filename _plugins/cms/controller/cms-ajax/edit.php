<?php

use CMS\Droplet;
use CMS\Util;

# Check we have POST data
if ( !is_form_data() || get_request_method() !== 'POST' ) {
    JSON::parse( 100, 'negative', 'No data', null, true );
}

# Types
$types = get('cms.types');

# Create a GUMP object
$gump = new GUMP();

# Sanitize Input
$data = form_data();

# Get Type
$type = $data['droplet_type'];
unset($data['droplet_type']);

# Check ID
if ( !isset($data['droplet_id']) ) {
    JSON::parse( 100, 'negative', 'Invalid ID', null, true );
}

# Get ID
$id = $data['droplet_id'];
unset($data['droplet_id']);

# Get Fields for Type
if ( array_key_exists($type, $types) ) {
    $fields = $types[$type]['fields'];
} else {
    JSON::parse( 100, 'negative', 'Invalid type', null, true );
}

# Pass data through GUMP for filter and validate
foreach ( $fields as $key => $field ) {

    # Remove empty image data
    if ( $field['type'] == 'cover_image' || $field['type'] == 'image' ) {
        unset($data[$field['type'] . '_image']);

        if ( !is_string($data[$field['type']]) || empty($data[$field['type']]) ) {
            unset($data[$field['type']]);
        }
    }

    # Title is always required for a Droplet
    if ( $key == 'title' ) {
        $validation[$key] = 'required';
    }

    # Validation Rules
    if ( array_key_exists('validate', $field) ) {
        $validation[$key] = $field['validate'];
    }

    # Filter Rules
    if ( array_key_exists('filter', $field) ) {
        $filter[$key] = $field['filter'];
    }

}

# Set Validation Rules
if ( isset($validation) && is_array($validation) ) {
    $gump->validation_rules($validation);
}

# Set Filter Rules
if ( isset($filter) && is_array($filter) ) {
    $gump->filter_rules($filter);
}

# Run GUMP
$clean = $gump->run($data);

# Pass errors back to user
if ( $clean === false ) {
    JSON::parse( 100, 'warning', $gump->get_readable_errors(true), null, true );
}

# Create Droplet object
$droplet = new Droplet($id);

# Set Title
$droplet->title = $clean['title'];
unset($clean['title']);

# Save the Droplet
if ( !$droplet->save() ) {
    JSON::parse( 100, 'negative', 'An error occured. Droplet was not saved', array(
        'droplet' => $droplet,
        'data' => $data,
        'clean' => $clean
    ), true );
}

# Trigger: cms_edit
do_trigger('cms_edit', $droplet);

# Loop through and add fields to the Droplet
foreach ( $clean as $key => $val ) {

    # If this is not a valid field for this type, skip it.
    if ( !array_key_exists($key, $fields) ) {
        continue;
    }

    # Update field object
    $droplet->$key = $val;

    # Set Status to 1
    $droplet->$key->publish();

    # If this is the title, make a slug with it
    if ( $key == 'title' && empty($droplet->slug) ) {
        $droplet->slug = Util::slug($val);
    }

    # Trigger: cms_edit_field
    do_trigger('cms_edit_field', $key, $droplet);

}

# Save the Droplet again if it changed
if ( $droplet->has_changed() ) {
    $droplet->save();
}

# Trigger: cms_save
do_trigger('cms_save', $droplet);

# All done!
JSON::parse( 200, 'positive', 'Saved!', array(
    'id' => $droplet->id
), true );
