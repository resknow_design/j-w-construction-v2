<?php

/**
 * CMS JSON
 * Droplet
 *
 * The Droplet endpoint provides an interface
 * for accessing content with an ID or Slug
 *
 * @method GET
 * @param id: Post to return (string|int)
 * @return Droplet
 */

use CMS\Droplet;

# Make sure request is GET
if ( get_request_method() !== 'GET' ) {
    JSON::parse( 401, 'negative', 'Forbidden: The droplet endpoint only accepts GET requests.', null, true );
}

# Create GUMP instance
$gump = new GUMP();

# Sanitize Query
$queries = $gump->sanitize($_GET);

if ( !isset($queries['id']) ) {
    JSON::parse( 401, 'negative', 'Forbidden: The droplet endpoint requires an ID.', null, true );
}

$droplet = new Droplet($queries['id']);

if ( $droplet->id != false ) {
    JSON::parse( 200, 'positive', 'OK', $droplet, true );
} else {
    JSON::parse( 404, 'negative', 'Not Found', null, true);
}
