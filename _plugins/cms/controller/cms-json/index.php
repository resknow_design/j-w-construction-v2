<?php

/**
 * CMS JSON
 * Index
 *
 * Displays a simple welcome message.
 */
JSON::parse( 200, 'positive', 'Welcome to the ' . get('site.company') . ' CMS JSON API', null, true );
