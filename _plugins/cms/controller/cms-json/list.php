<?php

/**
 * CMS JSON
 * List
 *
 * The List endpoint provides an interface
 * for accessing content of a specific type
 * by passing query strings.
 *
 * @method GET
 * @param type: Post type to return (blank for all)
 * @param limit: Number of posts to return
 * @param start: Where to start from
 * @return List of posts of queried type
 */

use CMS\Collection;

# Make sure request is GET
if ( get_request_method() !== 'GET' ) {
    JSON::parse( 401, 'negative', 'Forbidden: The list endpoint only accepts GET requests.', null, true );
}

$collection = new Collection();

# Create GUMP instance
$gump = new GUMP();

# Sanitize Query
$queries = $gump->sanitize($_GET);

# Build Query
$query = array();

# Get Post type
if ( isset($queries['type']) ) {
    $query['type'] = $queries['type'];
}

# Get Limit
if ( isset($queries['limit']) ) {
    $query['limit'] = $queries['limit'];
}

# Get Start
if ( isset($queries['start']) ) {
    $query['start'] = $queries['start'];
}

# Get Status
if ( isset($queries['status']) ) {
    $query['status'] = $queries['status'];
}

# Query the content
$content = $collection->get($query);

if ( is_array($content) ) {
    JSON::parse( 200, 'positive', 'OK', $content, true );
} else {
    JSON::parse( 404, 'negative', 'Not Found', null, true);
}
