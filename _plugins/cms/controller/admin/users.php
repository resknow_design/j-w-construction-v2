<?php

use CMS\Collection;

# Type
$type = 'user';

# Load All Content
$collection = new Collection($type);
$users = $collection->get(array(
    'status' => array(0, 1, 2)
));

# Set Template Variables
set('cms.list.content', $users);

# Set Data
set('cms.list.title', 'Users');
