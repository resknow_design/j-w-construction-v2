<?php

use CMS\User;
use CMS\Util;

$user = new User();

if ( cms_user_logged_in() ) {
    Util::redirect('/admin', true);
}
