<?php

use CMS\Collection;

# Load All Content
$collection = new Collection('field');
$posts = $collection->get(array(
    'droplet' => 0
));

# Set Template Variables
set('cms.list.content', $posts);

# Set Data
set('cms.list.type', 'global');
set('cms.list.title', 'Globals');
