<?php

use CMS\Collection;

# Load All Content
$collection = new Collection('field');
$posts = $collection->get(array(
    'type'  => array('cover_image', 'image')
));

# Set Template Variables
set('cms.list.content', $posts);

# Set Data
set('cms.list.title', 'Media');
