<?php

use CMS\Util;

# Get Post Types
$types = get('cms.types');

# Load Fields for this type
if ( isset($_GET['post_type']) ) {
    $type = strtolower($_GET['post_type']);
} else {
    $type = 'post';
}

# Check permission
$perm = ($type == 'page' ? 'page' : 'post');
if ( !user_can('add_' . $perm) ) {
    add_message('add-denied', 'negative', 'Sorry, you don\'t have permission to do that.');
    CMS::Util('location: /admin', true);
}

# Set Type
set('editor.type', $type);

# Sort fields by region
foreach ( $types[$type]['fields'] as $key => $field ) {
    set('editor.' . $field['region'] . '.' . $key, $field);
}
