<?php

use CMS\Droplet;
use CMS\Util;

# Get Post Types
$types = get('cms.types');

# Load Content
if ( isset($_GET['id']) ) {

    # Get content
    $content = new Droplet($_GET['id']);

    $perm = ($content->type == 'page' ? 'page' : 'post');

    # Permission check
    if ( !user_can('edit_' . $perm) && user_can('edit_own_' . $perm) && !$content->user_is_author() ) {
        add_message('edit-denied', 'negative', 'Sorry, you don\'t have permission to do that.');
        CMS::Util('location: /admin', true);

    # Check it's not empty
    } elseif ( $content->id > 0 ) {
        set('editor.content', $content);

        # Set Type
        set('editor.type', $content->type);

        # Sort fields by region
        foreach ( $types[$content->type]['fields'] as $key => $field ) {
            set('editor.' . $field['region'] . '.' . $key, $field);
        }
    }
}
