<?php

use CMS\Droplet;

# Load Content
if ( isset($_GET['id']) ) {

    # Get content
    $content = new Droplet($_GET['id']);

    # Check permission
    if ( !user_can('delete_post') && user_can('delete_own_post') && !$content->user_is_author() ) {
        add_message('delete-denied', 'negative', 'Sorry, you don\'t have permission to do that.');
    } elseif ( $content->id > 0 ) {
        $content->status = -1;
        $content->save();

        add_message('delete-success', 'positive', '<strong>' . $content->title . '</strong> has been deleted.');
    } else {
        add_message('delete-error', 'negative', 'There was an error deleting your post.');
    }

    header('location: /admin');
}
