<?php

use CMS\CMS;
use CMS\User;

// Check for User ID
if ( !isset( $_GET['id'] ) || !user_can('edit_user') ) {
    add_message('user-edit-denied', 'negative', 'Sorry, you don\'t have permission to do that.');
    CMS::Util('location: /admin', true);
}

// Get User Object
$edit = new User($_GET['id']);

// Check ID
if ( $edit->id == false ) {
    add_message('user-edit-not-found', 'negative', 'Sorry, we couldn\'t find that user.');
    CMS::Util('location: /admin', true);
}

// Set the user
set('editor.user', $edit);
