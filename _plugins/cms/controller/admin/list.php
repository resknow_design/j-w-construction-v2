<?php

use CMS\Collection;

# Type
$type = $_index[2];

# Query
$query = array(
    'type' => $type,
    'order' => 'DESC',
    'order_by' => 'date',
    'status' => array(0, 1)
);

# Load All Content
$collection = new Collection();
$posts = $collection->get($query);

# Set Template Variables
set('cms.list.content', $posts);

# Set Data
set('cms.list.type', $type);
set('cms.list.title', get('cms.types.' . $type . '.plural'));
