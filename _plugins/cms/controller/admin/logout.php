<?php

use CMS\CMS;
use CMS\Util;

if ( !cms_user_logged_in() ) {
    Util::redirect('/admin/login', true);
}

$cms = CMS::run();
$user = $cms->user();
$user->logout();

add_message( 'user-logout', 'positive', 'You have been logged out.' );

Util::redirect('/admin/login', true);
