<?php

use CMS\Util;

/**
 * This is just a redirect to the
 * update script.
 */
Util::redirect('location: /_plugins/cms/update.php');
