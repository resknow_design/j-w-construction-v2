<?php

namespace CMS;

class Collection {

    ### Properties
    ###----------------------
    public $type = 'droplet';
    private $last_query;
    private $table;
    private $props = array();
    private $db;

    public function __construct( $type = false ) {

        # Get Database instance
        $cms = CMS::run();
        $this->db = $cms->db();

        if ( $type == true ) {
            $this->type = $type;
        }

        # Set DB table
        $this->table = $this->type . 's';

        # Valid Properties
        $this->props = array(
            'droplet'  => array(
                'id',
                'parent',
                'date',
                'modified',
                'slug',
                'author',
                'type',
                'template',
                'status'
            ),
            'field'    => array(
                'id',
                'parent',
                'droplet',
                'slug',
                'type',
                'status'
            ),
            'user'     => array(
                'id',
                'username',
                'email',
                'role',
                'date',
                'modified',
                'status'
            )
        );

    }

    public function get( array $args = array() ) {

        # Default Args
        $defaults = array(
            'order' => 'DESC'
        );

        # Droplet defaults
        if ( $this->type == 'droplet' ) {
            $defaults['order_by'] = 'date';

        # Field defaults
        } else {
            $defaults['order_by'] = 'id';
        }

        # Merge Args
        $theArgs = array_merge($defaults, $args);

        # Database Args
        $dbArgs = array();

        # Database Options
        $dbOptions = array();

        # Filter Args to build a Medoo query
        foreach ( $theArgs as $arg => $val ) {
            switch (true) {

                # Limit
                case $arg === 'limit':
                    $dbArgs['LIMIT'] = $val;
                break;

                # Offset
                case $arg === 'start':
                case $arg === 'offset':
                    $dbArgs['OFFSET'] = $val;
                break;

                # Valid Property
                case $this->has_prop($arg):
                    $dbOptions[$arg] = $val;
                break;

            }
        }

        # Create "ORDER" key
        $dbArgs['ORDER'] = $theArgs['order_by'] . ' ' . $theArgs['order'];

        # Build Query
        switch (true) {

            # No arguments
            case empty($dbOptions):
                $where = array();
            break;

            # More than 1 arguments
            case count($dbOptions) > 1:
                $where = array(
                    'AND' => $dbOptions
                );
            break;

            # 1 argument
            default:
                $where = $dbOptions;
            break;

        }

        # Merge $dbArgs and $where
        $where = array_merge($dbArgs, $where);

        # Save this query
        $this->last_query = $where;

        # Query the database
        if ( $collection = $this->db->select($this->table, 'id', $where) ) {
            foreach ( $collection as $droplet ) {
                switch($this->type) {

                    case 'droplet':
                        $theCollection[$droplet] = new Droplet($droplet);
                    break;

                    case 'field':
                        $theCollection[$droplet] = new Field($droplet);
                    break;

                    case 'user':
                        $theCollection[$droplet] = new User($droplet);
                    break;

                }
            }

            return $theCollection;
        }

        return false;

    }

    private function has_prop( $prop ) {
        return in_array($prop, $this->props[$this->type]);
    }

}
