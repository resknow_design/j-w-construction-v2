<?php

namespace CMS;

use Exception;

class Droplet {

    ### Properties
    ###----------------------
    public $id;
    public $parent = 0;
    public $date;
    public $modified = null;
    public $slug;
    public $title;
    public $author = 0;
    public $type = 'post';
    public $template = 'post';
    public $status = 1;
    public $fields = array();
    private $has_changed = false;

    private $db; # Medoo instance

    /**
     * Construct
     *
     * @var (optional) $page (int|string) Page ID or Slug to load
     * @return void
     */
    public function __construct( $page = false ) {

        # Get Database instance
        $cms = CMS::run();
        $this->db = $cms->db();

        # Load Page if ID was supplied
        if ( $page == true ) {
            $this->load($page);
        }

    }

    /**
     * Get
     *
     * Get a property from this page
     * @see get()
     * @var $prop (string) Key of property to get
     * @return (mixed) Value of $prop
     */
    public function __get( $prop ) {
        return $this->get($prop);
    }

    /**
     * Set
     *
     * @see set()
     * @var $prop (string) Key of property to set
     * @var $val (mixed) Value of property
     * @return (self)
     */
    public function __set( $prop, $val ) {
        $this->set($prop, $val);
    }

    /**
     * Get
     *
     * Get a property from this page
     * @var $prop (string) Key of property to get
     * @return (mixed) Value of $prop
     */
    public function get( $prop ) {

        if ( $this->has_field($prop) ) {
            return $this->fields[$prop];
        } else {
            return (property_exists($this, $prop) ? $this->$prop : false);
        }

    }

    /**
     * Set
     *
     * @var $prop (string) Key of property to set
     * @var $val (mixed) Value of property
     * @return (self)
     */
    public function set( $prop, $val ) {

        # Don't allow modification of the ID
        if ( $prop === 'id' && $val !== 0 ) {
            throw new Exception('Cannot modify the ID once set. If you\'re trying to load a new Droplet, create a new instance.');
        }

        if ( $this->has_field($prop) ) {
            $this->fields[$prop]->value = $val;
        } elseif ( property_exists($this, $prop) ) {
            $this->$prop = $val;
        }

        $this->has_changed = true;
        return $this;

    }

    /**
     * Load
     *
     * Load a page in to this object
     * @var $page (int|string) Page ID or Slug
     * @return self if successful, false otherwise
     */
    public function load( $page ) {

        # Get $page Type
        if ( is_int($page) || is_numeric($page) ) {
            $type = 'id';
        } elseif ( is_string($page) ) {
            $type = 'slug';
        } else {
            throw new Exception(sprintf(
                'load(): %s is not a valid ID or Slug', $page
            ));
        }

        # Make sure we' don't query with an empty slug
        if ( $type === 'slug' && $page == '' ) {
            throw new Exception('load(): Querying for an empty slug is not possible.');
        }

        # Load Page from DB
        $page = $this->db->select('droplets', '*', array(
            $type   => $page
        ));

        # If the page loads, store it in this object.
        if ( !is_null($page) && !empty($page) ) {

            foreach ( $page[0] as $prop => $val ) {
                $this->$prop = $val;
            }

            # Load Fields
            $fields = $this->db->select('fields', 'id', array(
                'droplet'    => $this->id
            ));

            if ( !is_null($fields) && is_array($fields) ) {
                foreach ( $fields as $field ) {
                    $f = new Field();
                    $f->load($field);

                    if ( $f->id > 0 ) {
                        $this->fields[$f->slug] = $f;
                    }
                }
            }

            return $this;

        } else {
            return false;
        }

    }

    /**
     * Save
     *
     * Updates the current page in the database.
     * @return (bool) True if save was successful.
     */
    public function save() {

        # Check if Droplet is new
        if ( $this->id == false ) {
            return $this->add();
        }

        # Set modified time
        $this->modified = Util::timestamp();

        # Create an array to save in DB
        $data = array(
            'parent'    => $this->parent,
            'date'      => $this->date,
            'modified'  => $this->modified,
            'slug'      => $this->slug,
            'title'     => $this->title,
            'author'    => $this->author,
            'type'      => $this->type,
            'template'  => $this->template,
            'status'    => $this->status
        );

        # Save Fields
        foreach ( $this->fields as $field ) {
            $field->save();
        }

        # Save Changes
        return $this->db->update('droplets', $data, array(
            'id'    => $this->id
        ));

    }

    /**
     * Add
     *
     * Saves this Droplet as new droplet
     */
    public function add() {

        # Stop existing Droplet being re-created
        if ( $this->id == true ) {
            throw new Exception('You can\'t use the add() method on an existing Droplet');
        }

        # If Slug if empty, create an empty string
        if ( $this->slug == false ) {
            $this->slug = '';
        }

        # Set the author ID
        if ( $this->author == 0 ) {
            $this->author = cms_get_user();
        }

        # Create an array to save in DB
        $data = array(
            'parent'    => $this->parent,
            'slug'      => $this->slug,
            'title'     => $this->title,
            'author'    => $this->author,
            'type'      => $this->type,
            'template'  => $this->template,
            'status'    => $this->status
        );

        if ( $id = $this->db->insert('droplets', $data) ) {
            $this->id = $id;
            return true;
        }

        return false;


    }

    /**
     * Publish
     *
     * Change post status to published
     * @return (self)
     */
    public function publish() {
        $this->status = 1;
        return $this;
    }

    /**
     * Unpublish
     *
     * Change post status to unpublished
     * @return (self)
     */
    public function unpublish() {
        $this->status = 0;
        return $this;
    }

    /**
     * Delete
     *
     * Change post status to deleted
     * @return (self)
     */
    public function delete() {
        $this->status = -1;
        return $this;
    }

    ### Helpers
    ###-------------------

    /**
     * Has Field
     *
     * @var $field (string): Field key
     * @return (bool) True if post has field with key passed.
     */
    public function has_field($field) {
        return array_key_exists($field, $this->fields);
    }

    /**
     * Has Fields
     *
     * @return (bool) True if post has fields
     */
    public function has_fields() {
        return !empty($this->fields);
    }

    /**
     * Has Children
     *
     * @return (bool) True if post has children
     */
    public function has_children() {
        return $this->db->has('droplets', array('parent' => $this->id));
    }

    /**
     * Is Child
     *
     * @return (bool) True if post has a parent
     */
    public function is_child() {
        return $this->parent > 0;
    }

    /**
     * Is Published
     *
     * @return (bool) True if post is published
     */
    public function is_published() {
        return $this->status === 1 && strtotime($this->date) >= time();
    }

    /**
     * Is Scheduled
     *
     * @return (bool) True if post is published & has a date in the future
     */
    public function is_scheduled() {
        return $this->status === 1 && strtotime($this->date) < time();
    }

    /**
     * Is Deleted
     *
     * @return (bool) True if post is deleted
     */
    public function is_deleted() {
        return $this->status === -1;
    }

    /**
     * Is Modified
     *
     * @return (bool) True if post has been modified
     */
    public function is_modified() {
        return $this->modified == true;
    }

    /**
     * Has Changed
     *
     * @return (bool) True if post object has been modified since
     * load or last save.
     */
    public function has_changed() {
        return $this->has_changed == true;
    }

    /**
     * Is Slug
     *
     * @return (bool) True if post has a slug.
     */
    public function has_slug() {
        return $this->slug == true;
    }

    /**
     * User Is Author
     *
     * @return (bool) True if current user is author
     */
    public function user_is_author() {
        return $this->author === cms_get_user();
    }

}
