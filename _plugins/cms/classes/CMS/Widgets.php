<?php

namespace CMS;

use Exception;

class Widgets {

    ### Properties
    ###------------------
    private static $instance;
    private $widgets = array();

    /**
     * Construct
     */
    private function __construct() {
        $this->update_variable();
    }

    /**
     * Add Widget
     *
     * @param $id (string) Unique ID of widget
     * @param $region (string) Dashboard region (left, middle, right)
     * @param $content (array) Array of widget content parts
     *
     * Example:
     * $content = array(
     *     'title'    => 'My Widget',
     *     'content'  => '<p>This widget does nothing</p>',
     *     'footer'   => 'Oh look at this footer!'
     * );
     *
     * @param $update (bool) False by default, used by update_widget()
     * @return void
     */
    public function add_widget( $id, $region, array $content = array(), $update = false ) {

        # Validate id
        if ( !is_string($id) ) {
            throw new Exception('Widget IDs must be strings');
        }

        # Make sure $content has at least content filled in.
        if ( !array_key_exists( 'content', $content ) ) {
            throw new Exception('Widgets require content.');
        }

        # Lowercase the ID
        $id = strtolower($id);

        # Build Widget
        $widget = array(
            'id'        => $id,
            'region'    => $region,
            'content'   => $content
        );

        # Filter: cms_[add|update]_widget
        if ( $update === true ) {
            $widget = apply_filters('cms_update_widget', $widget);
        } else {
            $widget = apply_filters('cms_add_widget', $widget);
        }

        # Save
        $this->widgets[$region][$id] = $widget;

        # Update Variable
        $this->update_variable();

    }

    /**
     * Update Widget
     *
     * @param $id (string) Unique ID of widget
     * @param $region (string) Dashboard region (left, middle, right)
     * @param $content (array) Array of widget content parts
     *
     * @return void
     */
    public function update_widget( $id, $region, array $content = array() ) {
        return $this->add_widget( $id, $region, $content, $update = true );
    }

    /**
     * Remove Widget
     *
     * @param $id (string) Widget ID
     */
    public function remove_widget( $id ) {

        # Check Widget Exists
        if ( !$this->widget_exists($id) ) {
            return false;
        }

        # Remove Widget
        unset($this->widgets[$id]);

        # Update Variable
        $this->update_variable();

        return true;

    }

    /**
     * Get Widgets
     */
    public function get_widgets() {
        return $this->widgets;
    }

    /**
     * Update Variable
     *
     * Update the cms.widget variable
     */
    private function update_variable() {
        set('cms.widgets', $this->widgets);
    }

    /**
     * Widget Exists
     *
     * @param $id (string) Widget ID
     */
    public function widget_exists( $id ) {
        return array_key_exists( $id, $this->widgets );
    }

    /**
     * Run
     *
     * Return a singleton instance of Widgets
     */
    public static function run() {

        if ( self::$instance === null ) {
            self::$instance = new Widgets();
        }

        return self::$instance;

    }

}
