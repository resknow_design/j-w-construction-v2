<?php

namespace CMS;

use medoo;
use Exception;

class Field {

    ### Properties
    ###----------------------
    public $id;
    public $parent = 0;
    public $droplet = 0;
    public $slug;
    public $type = 'text';
    public $status = 1;
    public $value = '';
    public $children = null;
    private $has_changed = false;
    private $db;

    /**
     * Construct
     *
     * @var (optional) $field (int|string) Page ID or Slug to load
     * @return void
     */
    public function __construct( $field = false ) {

        # Get Database instance
        $cms = CMS::run();
        $this->db = $cms->db();

        # Load Page if ID was supplied
        if ( $field == true ) {
            $this->load($field);
        }

    }

    /**
     * To String
     *
     * @return The value of this field
     */
    public function __toString() {
        return $this->value;
    }

    /**
     * Get
     *
     * Get a property from this page
     * @see get()
     * @var $prop (string) Key of property to get
     * @return (mixed) Value of $prop
     */
    public function __get( $prop ) {
        return $this->get($prop);
    }

    /**
     * Set
     *
     * @see set()
     * @var $prop (string) Key of property to set
     * @var $val (mixed) Value of property
     * @return (self)
     */
    public function __set( $prop, $val ) {
        $this->set($prop, $val);
    }

    /**
     * Get
     *
     * Get a property from this page
     * @var $prop (string) Key of property to get
     * @return (mixed) Value of $prop
     */
    public function get( $prop ) {
        return (property_exists($this, $prop) ? $this->$prop : false);
    }

    /**
     * Set
     *
     * @var $prop (string) Key of property to set
     * @var $val (mixed) Value of property
     * @return (self)
     */
    public function set( $prop, $val ) {

        if ( property_exists($this, $prop) ) {
            $this->$prop = $val;
        }

        $this->has_changed = true;
        return $this;

    }

    /**
     * Load
     *
     * Load a page in to this object
     * @var $field (int|string) Page ID or Slug
     * @return self if successful, false otherwise
     */
    public function load( $field, array $options = array() ) {

        # Get $page Type
        if ( is_int($field) || is_numeric($field) ) {
            $type = 'id';
        } elseif ( is_string($field) ) {
            $type = 'slug';
        } else {
            throw new Exception(sprintf(
                'load(): %s is not a valid ID or Slug', $field
            ));
        }

        # Build query
        $query = array(
            $type => $field
        );

        # Merge with Options
        if ( !empty($options) ) {
            $query['AND'] = array_merge($query, $options);
            unset($query[$type]);
        }

        # Load Page from DB
        $field = $this->db->select('fields', '*', $query);

        # If the page loads, store it in this object.
        if ( !empty($field) && is_array($field) ) {

            foreach ( $field[0] as $prop => $val ) {
                $this->$prop = $val;
            }

            # Load Child fields
            if ( $this->has_children() ) {
                $children = $this->db->select('fields', 'id', array(
                    'parent'    => $this->id
                ));

                if ( !is_null($children) && is_array($children) ) {
                    foreach ( $children as $child ) {
                        $f = new Field($child);
                        $this->children[$f->slug] = $f;
                    }
                }
            }

            return $this;
        } else {
            return false;
        }

    }

    /**
     * Save
     *
     * Updates the current page in the database.
     * @return (bool) True if save was successful.
     */
    public function save() {

        # Check if Droplet is new
        if ( $this->id == false ) {
            return $this->add();
        }

        # Slug is Required
        if ( $this->slug == false ) {
            throw new Exception('Slug is required for new field');
        }

        # Create an array to save in DB
        $data = array(
            'parent'    => $this->parent,
            'droplet'   => $this->droplet,
            'slug'      => $this->slug,
            'type'      => $this->type,
            'status'    => $this->status,
            'value'     => $this->value
        );

        # Save Changes
        return $this->db->update('fields', $data, array(
            'id'    => $this->id
        ));

    }

    /**
     * Add
     *
     * Saves this Field as new droplet
     */
    public function add() {

        # Stop existing Droplet being re-created
        if ( $this->id == true ) {
            throw new Exception('You can\'t use the add() method on an existing Field');
        }

        # Slug is Required
        if ( $this->slug == false ) {
            throw new Exception('Slug is required for new field');
        }

        # Create an array to save in DB
        $data = array(
            'parent'    => $this->parent,
            'droplet'   => $this->droplet,
            'slug'      => $this->slug,
            'type'      => $this->type,
            'status'    => $this->status,
            'value'     => $this->value
        );

        if ( $id = $this->db->insert('fields', $data) ) {
            $this->id = $id;
            return true;
        }

        return false;


    }

    /**
     * Publish
     *
     * Change post status to published
     * @return (self)
     */
    public function publish() {
        $this->status = 1;
        return $this;
    }

    /**
     * Unpublish
     *
     * Change post status to unpublished
     * @return (self)
     */
    public function unpublish() {
        $this->status = 0;
        return $this;
    }

    /**
     * Delete
     *
     * Change post status to deleted
     * @return (self)
     */
    public function delete() {
        $this->status = -1;
        return $this;
    }

    ### Helpers
    ###-------------------

    /**
     * Has Children
     *
     * @return (bool) True if post has children
     */
    public function has_children() {
        return $this->db->has('fields', array('parent' => $this->id));
    }

    /**
     * Is Child
     *
     * @return (bool) True if post has a parent
     */
    public function is_child() {
        return $this->parent > 0;
    }

    /**
     * Is Global
     *
     * @return (bool) True if field has no Droplet (or 0)
     */
    public function is_global() {
        return $this->droplet === 0;
    }

    /**
     * Is Published
     *
     * @return (bool) True if post is published
     */
    public function is_published() {
        return $this->status === 1;
    }

    /**
     * Is Scheduled
     *
     * @return (bool) True if post is published & has a date in the future
     */
    public function is_scheduled() {
        return $this->status === 1;
    }

    /**
     * Is Deleted
     *
     * @return (bool) True if post is deleted
     */
    public function is_deleted() {
        return $this->status < 0;
    }

    /**
     * Is Modified
     *
     * @return (bool) True if post has been modified
     */
    public function is_modified() {
        return $this->modified == true;
    }

    /**
     * Has Changed
     *
     * @return (bool) True if post object has been modified since
     * load or last save.
     */
    public function has_changed() {
        return $this->hasChanged == true;
    }

    /**
     * Is Slug
     *
     * @return (bool) True if post has a slug.
     */
    public function has_slug() {
        return $this->slug == true;
    }

}
