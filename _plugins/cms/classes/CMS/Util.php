<?php

/**
 * CMS Utilities
 *
 * A group of static functions for
 * performing actions with CMS data
 */

namespace CMS;

class Util {

    /**
     * Timestamp
     *
     * Returns the current date in a MySQL timestamp format
     */
    public static function timestamp() {
        return date('Y-m-d H:i:s');
    }

    /**
     * Redirect
     *
     * Redirect a user to the URL passed
     * @param $destination (string) Destination to send the user to
     * @param $exit (bool) Stop the script at this point if true
     *
     * @NOTE The $exit param is not needed normally, it will
     * add an extra layer of security though in that if the
     * redirection fails, you can simply stop the script.
     */
    public static function redirect( $destination, $exit = false ) {

        # Trigger: cms_before_redirect
        do_trigger( 'cms_before_redirect', $destination );

        # Redirect
        header('location: ' . $destination);

        # Exit
        if ( $exit === true ) {
            exit;
        }

    }

    /**
     * Snippet
     *
     * Make a snippet from a string
     * @param $string (string) String to make snippet from
     * @param $length (int) Length of the snippet
     * @return (string) The snippet
     */
    public static function snippet( $string, $length = 150 ) {

            # Check $string is a string
            if ( !is_string($string) || strlen($string) <= 0 ) {
                throw new Exception('1st argument must be a string');
            }

            # Check $length is a number
            if ( !is_numeric($length) || $length < 0 ) {
                throw new Exception('Invalid string length');
            }

            # Strip HTML
            $string = strip_tags($string);

            # Create snippet
            $snippet = substr($string, 0, $length);

            # Append ellipsis
            if ( strlen($snippet) >= $length-3 ) {
                $snippet = $snippet .'...';
            }

            return $snippet;

    }

    /**
     * Slug
     *
     * Create a URL friendly slug from a string
     * @param $string (string) String to convert
     * @param $options (array) Optional array of options
     * @return (string) The slug
     */
    public static function slug( $string, $options = array() ) {

            # Set Options
        	$defaults = array(
        		'delimiter' => '-',
        		'limit' => null,
        		'lowercase' => true,
        		'replacements' => array(),
        		'transliterate' => false,
                'prefix' => ''
        	);

        	# Merge options
        	$options = array_merge($defaults, $options);

            # Make sure string is in UTF-8 and strip invalid UTF-8 characters
        	$string = mb_convert_encoding((string)$string, 'UTF-8', mb_list_encodings());

        	$char_map = array(
        		# Latin
        		'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
        		'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        		'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
        		'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
        		'ß' => 'ss',
        		'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
        		'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
        		'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
        		'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
        		'ÿ' => 'y',

        		# Latin symbols
        		'©' => '(c)',

        		# Greek
        		'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
        		'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
        		'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
        		'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
        		'Ϋ' => 'Y',
        		'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
        		'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
        		'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
        		'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
        		'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',

        		# Turkish
        		'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
        		'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',

        		# Russian
        		'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
        		'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
        		'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        		'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
        		'Я' => 'Ya',
        		'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
        		'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
        		'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        		'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
        		'я' => 'ya',

        		# Ukrainian
        		'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
        		'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',

        		# Czech
        		'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
        		'Ž' => 'Z',
        		'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
        		'ž' => 'z',

        		# Polish
        		'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
        		'Ż' => 'Z',
        		'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
        		'ż' => 'z',

        		# Latvian
        		'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
        		'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
        		'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
        		'š' => 's', 'ū' => 'u', 'ž' => 'z'
        	);

        	# Make custom replacements
        	$string = preg_replace(array_keys($options['replacements']), $options['replacements'], $string);

        	# Transliterate characters to ASCII
        	if ($options['transliterate']) {
        		$string = str_replace(array_keys($char_map), $char_map, $string);
        	}

        	# Replace non-alphanumeric characters with our delimiter
        	$string = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $string);

        	# Remove duplicate delimiters
        	$string = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $string);

        	# Truncate slug to max. characters
        	$string = mb_substr($string, 0, ($options['limit'] ? $options['limit'] : mb_strlen($string, 'UTF-8')), 'UTF-8');

        	# Remove delimiter from ends
        	$string = trim($string, $options['delimiter']);

            # Add Prefix
            if ( $options['prefix'] != '' ) {
                $string = $options['prefix'] . '/' . $string;
            }

        	return $options['lowercase'] ? mb_strtolower($string, 'UTF-8') : $string;

    }

}
