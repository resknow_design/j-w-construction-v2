<?php

namespace CMS;

use BP\Theme;
use BP\Controller;
use Spyc;
use medoo;
use Droplet;
use Field;

class CMS {

    ### Properties
    ###----------------------
    private static $instance;       # CMS Instance
    private $cms = array();         # CMS config
    private $user;                  # Current user
    private $path;                  # Current path
    private $index = array();       # Current path index array
    private $widgets;               # Dashboard Widgets (Widgets instance)
    private $db;                    # Database connection (Medoo instance)

    /**
     * Construct
     *
     * Runs the setup process for CMS including
     * creating a connection to the database,
     * creating a user object and loading content
     * types.
     *
     * @param $_path (string) Path variable from Boilerplate
     * @param $_index (array) Index array from Boilerplate
     * @param $_theme (Theme object) Instance of Theme from Boilerplate
     * @param $_controller (Controller object) Instance of Controller from Boilerplate
     * @return void
     */
    private function __construct() {

        # Trigger: cms_before_init
        do_trigger('cms_before_init');

        # Set Minimum Version
        plugin_requires_version('cms', '1.5.2');

        # Set Path & Index
        $this->path             = get('page.path');
        $this->index            = get('page.index');

        # Load Config
        $this->cms['config']    = plugin_load_config('cms');

        # Filter Config
        $this->cms['config']    = apply_filters('cms_config', $this->cms['config']);

        # Load Droplet Types
        $this->cms['types']     = $this->get_content_types();

        # Filter Types
        $this->cms['types']     = apply_filters('cms_types', $this->cms['types']);

        # Connect DB
        $this->db               = $this->db_init($this->cms['config']['db']);

        # Init Widgets
        $this->widgets          = Widgets::run();

        # Check Media Directory
        $this->check_media_dir();

        # Change theme and controller for defined paths
        if ( $this->path == 'admin' || path_contains('admin/') || path_contains('cms-ajax') || path_contains('cms-json') ) {

            use_theme( plugin_dir() . '/cms/ui' );
            use_controller( plugin_dir() . '/cms/controller' );

            # Set Theme Variables
            set('cms.config', $this->cms['config']);
            set('cms.types', $this->cms['types']);

        }

        # Autoload Content
        if ( isset($this->cms['config']['autoload']) && $this->cms['config']['autoload'] === true ) {
            $content = new Droplet($this->path);

            # Set Theme variables
            if ( !empty($content->id) ) {
                set('cms.content', $content);
            }
        }

        # Trigger: cms_init (CMS object passed)
        do_trigger('cms_init', $this);

    }

    /**
     * Config
     *
     * @return Config array
     */
    public function config() {
        return $this->config;
    }

    /**
     * Init DB
     *
     * Creates a new Medoo instance and
     * a connection to the database
     */
    private function db_init( array $creds = array() ) {

        if ( $this->db instanceof medoo ) {
            return $this->db;
        }

        # Make sure credentials is not empty
        if ( empty($creds) ) {
            throw new Exception('Database connection credentials are required');
        }

        # Create new Medoo instance
        $db = new medoo(array(
            'database_type'     => (isset($creds['type']) ? $creds['type'] : 'mysql'),
            'database_name'     => $creds['name'],
            'server'            => $creds['host'],
            'username'          => $creds['username'],
            'password'          => $creds['password'],
            'charset'           => (isset($creds['charset']) ? $creds['charset'] : 'utf8')
        ));

        return $db;

    }

    /**
     * DB
     *
     * Returns an instance of the CMS
     * Medoo connection
     */
    public function db() {

        if ( !$this->db instanceof medoo ) {
            $this->db = $this->db_init($this->cms['config']['db']);
        }

        return $this->db;
    }

    /**
     * User Init
     *
     * Create a new instance of CMSUser
     */
    private function user_init() {
        if ( cms_user_logged_in() ) {
            return new User(cms_get_user());
        } else {
            return new User();
        }
    }

    /**
     * User
     *
     * Return the current user object
     */
    public function user() {

        if ( !$this->user instanceof User ) {
            $this->user = $this->user_init();
        }

        return $this->user;
    }

    /**
     * Get Content Types
     *
     * Get defined content types from the
     * default config and user defined
     * config.
     */
    private function get_content_types() {

        # Load types from root
        if ( is_readable(ROOT_DIR . '/.types.yml') ) {
            return Spyc::YAMLLoad(ROOT_DIR . '/.types.yml');
        }

        # Load the defaults
        return Spyc::YAMLLoad(plugin_dir() . '/cms/.types.yml');

    }

    /**
     * Check Media Dir
     *
     * Performs a check on the media directory
     * to see if it exists and is writeable.
     *
     * If it doesn't exist, it will attempt to
     * create it.
     */
    private function check_media_dir() {

        $media_dir = ROOT_DIR . '/' . $this->cms['config']['media_dir'];

        # Check Media Directory exists
        if ( !is_dir($media_dir) ) {
            mkdir($media_dir);
        } elseif ( !is_writeable($media_dir) ) {
            throw new Exception('<strong>[CMS]</strong> Your media directory exists but is not writeable. Try <code>chmod 775</code>');
        }

    }

    /**
     * Run
     *
     * Get singleton instance of CMS
     */
    public static function run() {

        if ( self::$instance === null ) {
            self::$instance = new CMS();
        }

        return self::$instance;

    }

}
