<?php

namespace CMS;

use Exception;

class User {

    ### Properties
    ###----------------------
    public $id;
    public $username;
    public $password;
    public $email;
    public $role = 2;
    public $date;
    public $modified;
    public $status = 2;
    public $fields = array();
    private $db;
    private $has_changed;
    private $password_cache;

    /**
     * Construct
     *
     * @var (optional) $id (int|string) User ID or Username to load
     * @return void
     */
    public function __construct( $id = false ) {

        # Get Database instance
        $cms = CMS::run();
        $this->db = $cms->db();

        # Load Page if ID was supplied
        if ( $id == true ) {
            $this->load($id);
        }

    }

    /**
     * Get
     *
     * Get a property from this page
     * @see get()
     * @var $prop (string) Key of property to get
     * @return (mixed) Value of $prop
     */
    public function __get( $prop ) {
        return $this->get($prop);
    }

    /**
     * Set
     *
     * @see set()
     * @var $prop (string) Key of property to set
     * @var $val (mixed) Value of property
     * @return (self)
     */
    public function __set( $prop, $val ) {
        $this->set($prop, $val);
    }

    /**
     * Get
     *
     * Get a property from this page
     * @var $prop (string) Key of property to get
     * @return (mixed) Value of $prop
     */
    public function get( $prop ) {

        if ( $this->has_field($prop) ) {
            return $this->fields[$prop];
        } else {
            return (property_exists($this, $prop) ? $this->$prop : false);
        }

    }

    /**
     * Set
     *
     * @var $prop (string) Key of property to set
     * @var $val (mixed) Value of property
     * @return (self)
     */
    public function set( $prop, $val ) {

        # Don't allow modification of the ID
        if ( $prop === 'id' && $val !== 0 ) {
            throw new Exception('Cannot modify the ID once set. If you\'re trying to load a new User, create a new instance.');
        }

        if ( $this->has_field($prop) ) {
            $this->fields[$prop]->value = $val;
        } elseif ( property_exists($this, $prop) ) {
            $this->$prop = $val;
        }

        $this->has_changed = true;
        return $this;

    }

    /**
     * Load
     *
     * Load a page in to this object
     * @var $page (int|string) Page ID or Slug
     * @return self if successful, false otherwise
     */
    public function load( $user ) {

        # Get $page Type
        if ( is_int($user) || is_numeric($user) ) {
            $type = 'id';
        } elseif ( is_string($user) ) {
            $type = 'username';
        } else {
            throw new Exception(sprintf(
                'load(): %s is not a valid ID or Username', $user
            ));
        }

        # Make sure we' don't query with an empty slug
        if ( $type === 'username' && $user == '' ) {
            throw new Exception('load(): Querying for an empty slug is not possible.');
        }

        # Load Page from DB
        $user = $this->db->select('users', '*', array(
            $type   => $user
        ));

        # If the page loads, store it in this object.
        if ( !is_null($user) && !empty($user) ) {

            foreach ( $user[0] as $prop => $val ) {
                $this->$prop = $val;
            }

            # Cache Current Password
            $this->password_cache = $this->password;

            return $this;

        } else {
            return false;
        }

    }

    /**
     * Save
     *
     * Updates the current user in the database.
     * @return (bool) True if save was successful.
     */
    public function save() {

        # Check if Droplet is new
        if ( $this->id == false ) {
            return $this->add();
        }

        # Check password
        if ( $this->password !== $this->password_cache ) {
            $this->password = $this->generate_password( $this->password );
        }

        # Create an array to save in DB
        $data = array(
            'username'  => $this->username,
            'email'     => $this->email,
            'password'  => $this->password,
            'role'      => $this->role,
            'date'      => $this->date,
            'modified'  => $this->modified,
            'status'    => $this->status
        );

        # Save Fields
        if ( !empty($this->fields) ) {
            foreach ( $this->fields as $field ) {
                $field->save();
            }
        }

        # Save Changes
        return $this->db->update('users', $data, array(
            'id'    => $this->id
        ));

    }

    /**
     * Add
     *
     * Saves this Droplet as new droplet
     */
    public function add() {

        # Stop existing Droplet being re-created
        if ( $this->id == true ) {
            throw new Exception('You can\'t use the add() method on an existing User');
        }

        # If Username is empty, stop.
        if ( $this->username == false ) {
            throw new Exception('Username cannot be blank.');
        }

        # Generate a password
        $this->password = $this->generate_password( $this->password );

        # Create an array to save in DB
        $data = array(
            'username'  => $this->username,
            'email'     => $this->email,
            'password'  => $this->password,
            'role'      => $this->role,
            'status'    => $this->status
        );

        if ( $id = $this->db->insert('users', $data) ) {
            $this->id = $id;
            return true;
        }

        return false;


    }

    /**
     * Activate
     *
     * Change post status to activated
     * @return (self)
     */
    public function activate() {
        $this->status = 1;
        return $this;
    }

    /**
     * Deactivate
     *
     * Change post status to deactivated
     * @return (self)
     */
    public function deactivate() {
        $this->status = 0;
        return $this;
    }

    /**
     * Delete
     *
     * Change post status to deleted
     * @return (self)
     */
    public function delete() {
        $this->status = -1;
        return $this;
    }

    /**
     * Is Active
     *
     * @return (bool) True if post is published
     */
    public function is_active() {
        return $this->status === 1 || $this->status === 2;
    }

    /**
     * Is New
     *
     * @return (bool) True is user is new (Status 2)
     */
    public function is_new() {
        return $this->status == 2;
    }

    /**
     * Is Deleted
     *
     * @return (bool) True if post is deleted
     */
    public function is_deleted() {
        return $this->status === -1;
    }

    /**
     * Is Modified
     *
     * @return (bool) True if post has been modified
     */
    public function is_modified() {
        return $this->modified == true;
    }

    /**
     * Has Changed
     *
     * @return (bool) True if post object has been modified since
     * load or last save.
     */
    public function has_changed() {
        return $this->has_changed == true;
    }

    /**
     * Is Current User
     */
    public function is_current_user() {
        return $this->id === cms_get_user();
    }

    /**
     * Can
     *
     * Check if this user has permission
     * @param $perm (string) Permission to check
     * @return (bool) True/false
     */
    public function can( $perm ) {

        # Validate $perm
        if ( !is_string($perm) ) {
            return false;
        }

        # Get Role
        $role = $this->db->select('roles', '*', array(
            'id'    => $this->role
        ));

        # Make sure $role is not empty
        if ( empty($role) ) {
            return false;
        }

        # Get role info
        $role = $role[0];

        # Get permissions
        $perms = explode(', ', $role['permissions']);

        # Check permission
        return in_array( strtolower($perm), $perms );

    }

    /**
     * Login
     */
    public function login( $username, $password ) {

        # Get User
        $this->load($username);

        # Check user was loaded
        if ( $this->id == false ) {
            throw new Exception('User not found');
        }

        if (hash_equals($this->password, crypt($password, $this->password))) {
            $_SESSION['cms_user'] = $this->id;
            return true;
        } else {
            return false;
        }

    }

    public function change_password( $password ) {
        $this->password = $this->generate_password($password);
    }

    /**
     * Generate Password
     *
     * Create a hash of a plain text string
     * @param $password (string)
     */
    private function generate_password( $password ) {

        # Validate String
        if ( !is_string( $password ) ) {
            throw new Exception('Password is not a string.');
        }

        # Generate hash
        if (defined('CRYPT_BLOWFISH') && CRYPT_BLOWFISH) {
            $salt = '$2y$14$' . substr(sha1(uniqid(rand(), true)), 0, 22);
            return crypt($password, $salt);
        }

    }

    /**
     * Logout
     *
     * Destroy the user session
     */
    public function logout() {

        # Check user is logged in
        if ( cms_get_user() !== $this->id )
            return false;

        # Log the user out
        unset($_SESSION['cms_user']);

    }

    /**
     * Has Field
     *
     * @var $field (string): Field key
     * @return (bool) True if post has field with key passed.
     */
    public function has_field($field) {
        return array_key_exists($field, $this->fields);
    }

    /**
     * Has Fields
     *
     * @return (bool) True if post has fields
     */
    public function has_fields() {
        return !empty($this->fields);
    }

}
