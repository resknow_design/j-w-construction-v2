<?php

/**
 * CMS Plugin for Boilerplate
 *
 * @version 1.0.0 Beta
 * @package Boilerplate
 * @author Chris Galbraith
 */

use CMS\CMS;
use CMS\Util;
use CMS\User;

# Include required Classes & Functions
require_once __DIR__ . '/lib/includes.php';

# Run CMS Object
CMS::run();

# Include functions
require_once __DIR__ . '/functions/droplet.php';

# Check login status
if ( path_contains('admin') && !is_path('admin/login') && !is_path('admin/password') && !cms_user_logged_in() ) {
    Util::redirect('/admin/login');
}
