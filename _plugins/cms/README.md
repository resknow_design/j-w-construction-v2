# CMS for Boilerplate

CMS is a flexible, easy to use plugin for our [Boilerplate](https://github.com/resknow/boilerplate). Out of the box, it supports Filters & Triggers for customisation, Automatic Updates (CRON required), Custom Post Types, 12 Field Types and has a basic API implementation.

CMS for Boilerplate makes very little assumptions about how you want to display your content, as such, it is not a set and forget product. It handles the content creation and storage, the rest is pretty much up to you.

## Documentation

Full docs coming soon, in the meantime, here's a quick reference of Triggers and Filters that you can hook in to.

### Triggers
- `cms_before_init` - `classes/CMS/CMS.php` Fired right before the CMS object is instantiated.
- `cms_init( object $this )` - `classes/CMS/CMS.php` Fired right after the CMS object is instantiated. The object is passed in as an argument.
- `cms_before_redirect( string $destination )` - `classes/CMS/Util.php` Fired right before executing the `redirect()` method in Util. The destination is passed in as a string.
- `cms_edit( object $droplet )` - `controller/cms-ajax/edit.php` Fired after editing a Droplet, but before editing any fields. Droplet object is passed in.
- `cms_edit_field( string $key, object $droplet )` - `controller/cms-ajax/edit.php` Fired after editing a Field's value. The Field's key is passed in as a string and the current Droplet is passed in as an object.
- `cms_save( object $droplet )` - `controller/cms-ajax/edit.php` Fired after editing a Droplet is complete and all fields have updated.
- `cms_user_login( object $user )` - `controller/cms-ajax/login.php` Fired on user login. The User object is passed in as an argument.

### Filters
- `cms_config( array $this->cms['config'] )` - `classes/CMS/CMS.php` Applied during instantiation of the CMS object. CMS Config loaded from .config.yml is passed in as an array.
- `cms_types( array $this->cms['types'] )` - `classes/CMS/CMS.php` Applied during instantiation of the CMS object. CMS Post Types loaded from .types.yml are passed in as an array.
- `cms_update_widget( array $widget )` - `classes/CMS/Widget.php` Applied while updating a Dashboard Widget. The Widget is passed in as an array.
- `cms_add_widget( array $widget )` - `classes/CMS/Widget.php` Applied while adding a new Dashboard Widget. The Widget is passed in as an array.

### Field Types

- `checkbox` An HTML checkbox field
- `colour` An HTML color field
- `cover_image` A wrapper for the `image` field, used by default Droplet type for Cover Images.
- `email` An HTML e-mail field
- `image` An image upload field
- `number` An HTML number field
- `repeater` (in development, not stable) A container field for repeating other fields.
- `select` An HTML select dropdown
- `text` An HTML text field
- `textarea` An HTML textarea field
- `title` A title field, used for the Droplet title
- `wysiwyg` A WYSIWYG textarea field, powered by Aloha Editor
