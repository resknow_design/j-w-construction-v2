<?php

/**
 * User Can
 *
 * @param $perm (string) Permission to check
 * @param $user (int) (optional) User ID, defaults to current user
 */
function user_can( $perm, $user = false ) {

    # Get CMS instance
    $cms = CMS\CMS::run();

    # Get User
    if ( $user === false ) {
        $the_user = $cms->user();
    } else {
        $the_user = new CMS\User($user);
    }

    # Run check
    return $the_user->can($perm);

}
