<?php

/**
 * Get Droplet
 *
 * Load a droplet by ID or Slug
 *
 * @param $id (string|int) ID or slug
 * @return Droplet object
 */
function get_droplet( $id ) {
    return new CMS\Droplet($id);
}
