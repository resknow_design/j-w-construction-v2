<?php

/**
 * Add Message
 *
 * Save a message to show on the dashboard screen
 * @param $id (string) Unique, lowercase ID for this message
 * @param $type (string) Message type (positive, negative, warning)
 * @param $message (string) Message content
 */
function add_message( $id, $type, $message ) {

    # Check $id is a string
    if ( !is_string($id) )
        return false;

    # Make $id lowercase
    $id = strtolower($id);

    # Save the message
    $_SESSION['cms_messages'][$id] = array(
        'type'      => $type,
        'message'   => $message
    );

    return true;

}

/**
 * Remove Message
 *
 * Remove a message from the cms_messages array
 * @param $id (string) ID of the message to remove
 */
function remove_message( $id ) {

    if ( isset($_SESSION['cms_messages'][$id]) ) {
        unset($_SESSION['cms_messages'][$id]);
    }

}

/**
 * Get Messages
 *
 * Return an array of messages
 */
function get_messages() {

    if ( isset($_SESSION['cms_messages']) ) {
        return $_SESSION['cms_messages'];
    }

    return false;

}
