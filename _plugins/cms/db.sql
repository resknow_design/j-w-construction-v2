SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `droplets` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT 'page',
  `template` varchar(255) NOT NULL DEFAULT 'page',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `droplets` (`id`, `parent`, `date`, `modified`, `slug`, `title`, `author`, `type`, `template`, `status`) VALUES
(1, 0, '2016-03-03 12:32:12', '2016-03-08 14:03:20', 'hello-world', 'Hello World', 1, 'post', 'post', 1);

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `droplet` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'text',
  `status` int(11) NOT NULL DEFAULT '1',
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `fields` (`id`, `parent`, `droplet`, `slug`, `type`, `status`, `value`) VALUES
(1, 0, 1, 'cover_image', 'cover_image', -1, ''),
(2, 0, 1, 'content', 'wysiwyg', 1, '<p>This is your first blog post. Edit it or delete it and start posting your own content.</p>');

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `permissions` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`) VALUES
(1, 'admin', 'Admin', 'add_user, edit_user, delete_user, edit_own_user, add_post, edit_post, delete_post, edit_own_post, delete_own_post, add_page, edit_page, delete_page, edit_own_page, delete_own_page, add_global, edit_global, delete_global, edit_own_global, delete_own_global'),
(2, 'client', 'Client', 'add_post, edit_post, delete_post, edit_own_post, delete_own_post, edit_page');

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`, `date`, `modified`, `status`) VALUES
(1, 'admin', '$2y$14$dd168b2407db9ff06da0feaJFQNVkZz.kF1ykNnngrvhjUWgrKgUS', 'studio@resknow.co.uk', 1, '2016-01-31 16:46:26', NULL, 1);

ALTER TABLE `droplets`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `droplets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
