# Install Boilerplate CMS

In the near future there will an installation process to run through, in the meantime, follow the steps below.

1. Create a MySQL database and import `db.sql` from the plugin directory.
2. Enter your MySQL database credentials in to `.config.yml` in the plugin directory.
3. Create a `_media` directory in your root directory and make sure it is writeable by the server `CHMOD 775`.
4. Login using username: `admin` and password `vuV8WRux`.
5. Check for updates by clicking 'Updates' from the sidebar menu.
6. Copy `.types.yml` to your root directory and define your post types.
7. Create client accounts by clicking 'Users' from the sidebar menu.

## Setup Automatic Updates

1. Make sure the whole `cms` directory is writeable by the server.
2. Create a CRON job to run ``<yoursite>/_plugins/cms/update.php` once a day.
