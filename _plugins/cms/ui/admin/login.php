<?php

set('page.title', 'Login - ' . get('site.company') . ' CMS');

get_partial('head'); ?>

    <section class="login-box">
        <div class="logo">
            <?php echo apply_filters('cms_login_logo', '<h1>' . get('site.company') . ' CMS</h1>'); ?>
        </div>
        <div class="card">
            <h3 class="title">Login</h3>
            <div class="content">
                <?php get_partial('messages'); ?>
                <form class="ajax-form" action="/cms-ajax/login" method="post" data-push="/admin">
                    <div class="field">
                        <label for="username">Username</label>
                        <input type="text" name="username">
                    </div>
                    <div class="field">
                        <label for="password">Password</label>
                        <input type="password" name="password">
                    </div>
                    <div class="field">
                        <button class="positive button" type="submit">Login</button>
                        <button class="request-new-password button float-right a-button" type="button" data-href="/admin/password">Request new password</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
