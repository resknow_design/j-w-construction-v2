<?php

set('page.title', 'Request a new Password - ' . get('site.company') . ' CMS');

get_partial('head'); ?>

    <section class="login-box">
        <div class="logo">
            <?php echo apply_filters('cms_login_logo', '<h1>' . get('site.company') . ' CMS</h1>'); ?>
        </div>
        <div class="card">
            <h3 class="title">Request a new Password</h3>
            <div class="content">
                <form>
                    <?php echo apply_filters('cms_request_password', '<p>Please call the tech support number given to you when you placed your order and we\'ll organise a new password for you.</p><p style="color: #777"><em>Please note this may take up to 2 working days as passwords are reset manually for added security.</em></p>'); ?>
                </form>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
