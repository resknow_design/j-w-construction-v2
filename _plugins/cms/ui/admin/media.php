<?php

set('page.title', 'Media Library | ' . get('site.company') . ' CMS');

get_header(); ?>

    <div class="container">

        <section class="page-title">
            <h1>Media</h1>
        </section>

        <?php get_partial('messages'); ?>

        <?php if ( $media = get('cms.list.content') ): ?>

            <div class="row">
                <?php foreach ( $media as $item ): ?>
                <div class="col-12 col-small-6 col-medium-4 col-large-3 margin-top-2">
                    <a class="card" href="<?php echo $item->value; ?>">
                        <div class="image">
                            <img src="<?php echo $item->value; ?>">
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>

        <?php else: ?>

            <div class="card">
                <div class="strong content padding-top-2 padding-bottom-2">
                    <p>There are no Media items to show.</p>
                </div>
            </div>

        <?php endif; ?>

    </div>

<?php get_footer(); ?>
