<?php

set('page.title', 'Edit | ' . get('site.company') . ' CMS');

$content = get('editor.content');

get_header(); ?>

<div class="app-container">
    <form id="editor-form" class="ajax-form" action="/cms-ajax/edit" method="post" enctype="multipart/form-data">
        <input type="hidden" name="droplet_type" value="<?php echo get('editor.type'); ?>" />
        <input type="hidden" name="droplet_id" value="<?php echo $content->id; ?>" />

        <nav class="editor-sidebar">

            <header>
                <h2 class="cck-truncate">
                    <a href="/admin">
                        <?php if ( $logo = get('cms.config.logo') ): ?>
                            <span class="site-logo" style="background-image: url('<?php echo $logo; ?>')"></span>
                        <?php endif; ?>
                        <?php echo get('site.company'); ?>
                    </a>
                </h2>
            </header>

            <section class="actions">
                <div class="button-group">
                    <button class="large primary button action-preview" type="submit"><i class="material-icons margin-right-small">save</i> Save</button>
                </div>
            </section>

            <?php do_trigger('cms_editor_sidebar'); ?>

            <?php

            ### Include Sidebar fields

            # Include the template directly so we keep
            # the variable scope intact.
            if ( $sidebar = get('editor.sidebar') ) {
                foreach ( $sidebar as $key => $field ) {
                    include plugin_dir() . '/cms/ui/fields/' . $field['type'] . '.php';
                }
            }

            ?>
        </nav>

        <div class="medium container">

            <?php get_partial('messages'); ?>

            <section class="cms-editor">
                <?php

                ### Include Editor fields

                # Include the template directly so we keep
                # the variable scope intact.
                if ( $editor = get('editor.editor') ) {
                    foreach ( $editor as $key => $field ) {
                        include plugin_dir() . '/cms/ui/fields/' . $field['type'] . '.php';
                    }
                }

                ?>
            </section>
        </div>

    </form>
</div>

<?php get_footer(); ?>
