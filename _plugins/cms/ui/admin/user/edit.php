<?php

set('page.title', 'New User | ' . get('site.company') . ' CMS');

$user = get('editor.user');

get_header(); ?>

<div class="app-container">
    <form class="ajax-form" action="/cms-ajax/user-edit" method="post" data-push="this" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $user->id; ?>">

        <div class="medium container">

            <section class="page-title">
                <h1>Edit <?php echo $user->username ;?></h1>
                <h4 class="sub-heading">Edit a CMS user</h4>
            </section>

            <?php get_partial('messages'); ?>

            <section class="margin-top-4">

                    <div class="card">
                        <div class="content">
                            <h3 class="title">User Information</h3>
                            <div class="row">
                                <div class="col-12 col-medium-7">
                                    <div class="field">
                                        <input type="text" name="username" placeholder="Username"<?php if ( !empty($user->username) ): ?> value="<?php echo $user->username ;?>"<?php endif; ?>>
                                    </div>
                                </div>
                                <div class="col-12 col-medium-5">
                                    <div class="field">
                                        <p>Change username</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-medium-7">
                                    <div class="field">
                                        <input type="password" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-12 col-medium-5">
                                    <div class="field">
                                        <p>Change password<br><em class="color-grey">Leave blank to keep the same.</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-medium-7">
                                    <div class="field">
                                        <input type="email" name="email" placeholder="E-mail address"<?php if ( !empty($user->email) ): ?> value="<?php echo $user->email ;?>"<?php endif; ?>>
                                    </div>
                                </div>
                                <div class="col-12 col-medium-5">
                                    <div class="field">
                                        <p>Change e-mail address</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <h3 class="title">Options</h3>
                            <div class="row">
                                <div class="col-12 col-medium-6">
                                    <div class="field">
                                        <label>Role</label>
                                        <select name="role"<?php if ( $user->id == '1' ): ?> disabled="true"<?php endif; ?>>
                                            <option value="1"<?php if ( $user->role == '1' ): ?> selected<?php endif; ?>>Admin</option>
                                            <option value="2"<?php if ( $user->role == '2' ): ?> selected<?php endif; ?>>Client</option>
                                        </select>
                                        <p class="description"><i class="color-negative material-icons">info</i> Changing this may have security implications.</p>
                                    </div>
                                </div>
                                <div class="col-12 col-medium-6">
                                    <div class="field">
                                        <label>Status</label>
                                        <select name="role">
                                            <option value="2"<?php if ( $user->status == '2' ): ?> selected<?php endif; ?>>New</option>
                                            <option value="1"<?php if ( $user->status == '1' ): ?> selected<?php endif; ?>>Active</option>
                                            <option value="0"<?php if ( $user->status == '0' ): ?> selected<?php endif; ?>>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <button class="positive button" type="submit">Save</button>
                            <button class="button float-right" type="reset">Cancel</button>
                        </div>
                    </div>

            </section>
        </div>

    </form>
</div>

<?php get_footer(); ?>
