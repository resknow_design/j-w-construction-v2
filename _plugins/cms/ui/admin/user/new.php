<?php

set('page.title', 'New User | ' . get('site.company') . ' CMS');

get_header(); ?>

<div class="app-container">
    <form class="ajax-form" action="/cms-ajax/user-new" method="post" enctype="multipart/form-data">

        <div class="medium container">

            <section class="page-title">
                <h1>New User</h1>
                <h4 class="sub-heading">Add a new CMS user</h4>
            </section>

            <?php get_partial('messages'); ?>

            <section class="margin-top-4">

                    <div class="card">
                        <div class="content">
                            <h3 class="title">User Information</h3>
                            <div class="row">
                                <div class="col-12 col-medium-7">
                                    <div class="field">
                                        <input type="text" name="username" placeholder="Username">
                                    </div>
                                </div>
                                <div class="col-12 col-medium-5">
                                    <div class="field">
                                        <p>Choose a username</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-medium-7">
                                    <div class="field">
                                        <input type="password" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-12 col-medium-5">
                                    <div class="field">
                                        <p>Choose a password</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-medium-7">
                                    <div class="field">
                                        <input type="email" name="email" placeholder="E-mail address">
                                    </div>
                                </div>
                                <div class="col-12 col-medium-5">
                                    <div class="field">
                                        <p>Enter an e-mail address</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <h3 class="title">Options</h3>
                            <div class="row">
                                <div class="col-12 col-medium-6">
                                    <div class="field">
                                        <label>Role</label>
                                        <select name="role">
                                            <option value="1">Admin</option>
                                            <option value="2">Client</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-medium-6">
                                    <div class="field">
                                        <label>Status</label>
                                        <select name="role">
                                            <option value="2">New</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <button class="positive button" type="submit">Save</button>
                            <button class="button float-right" type="reset">Cancel</button>
                        </div>
                    </div>

            </section>
        </div>

    </form>
</div>

<?php get_footer(); ?>
