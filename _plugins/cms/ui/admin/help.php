<?php

set('page.title', 'Help | ' . get('site.company') . ' CMS');

get_header(); ?>

    <div class="container">

        <section class="page-title">
            <h1>Help</h1>
        </section>

        <section class="dashboard-content">

            <?php get_partial('messages'); ?>

            <div class="row">

                <div class="col-12 col-medium-4">

                    <div class="help-card card">
                        <div class="content">
                            <h3 class="title">Posts</h3>
                            <ul>
                                <li>
                                    <a href="#">How to add a Post</a>
                                    <div class="answer">
                                        <p>To add a new Post, follow these steps:</p>
                                        <ol>
                                            <li>1. From the sidebar, click on New Post</li>
                                            <li>2. Fill in the Title field</li>
                                            <li>3. Write your post</li>
                                            <li>4. Click Save</li>
                                        </ol>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">How to edit a Post</a>
                                    <div class="answer">
                                        <p>To edit a Post, select Posts from the sidebar, then hover your mouse over the title of the Post you want to edit, then click Edit.</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">How to delete a Post</a>
                                    <div class="answer">
                                        <p>To delete a Post, select Posts from the sidebar, then hover your mouse over the title of the Post you want to delete, then click Delete.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <?php do_trigger('cms_help_left'); ?>

                </div>

                <div class="col-12 col-medium-4">

                    <div class="help-card card">
                        <div class="content">
                            <h3 class="title">Pages</h3>
                            <ul>
                                <li>
                                    <a href="#">How to edit a Page</a>
                                    <div class="answer">
                                        <p>To edit a Page, select Pages from the sidebar, then hover your mouse over the title of the Page you want to edit, then click Edit.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <?php do_trigger('cms_help_middle'); ?>

                </div>

                <div class="col-12 col-medium-4">

                    <?php do_trigger('cms_help_right'); ?>

                </div>

            </div>

        </section>

    </div>

<?php get_footer(); ?>
