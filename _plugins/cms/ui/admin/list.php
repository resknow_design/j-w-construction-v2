<?php

set('page.title', 'Pages | ' . get('site.company') . ' CMS');
$type = get('cms.list.type');

get_header(); ?>

    <div class="container">

        <section class="page-title">
            <h1><?php echo get('cms.list.title'); ?></h1>
        </section>

        <?php get_partial('messages'); ?>

        <?php if ( $pages = get('cms.list.content') ): ?>
        <div class="post-list card">
            <header class="header">
                <div class="strong row">
                    <div class="post-title col-12 col-medium-8">
                        Title
                    </div>
                    <div class="post-date col-12 col-medium-2">
                        Date
                    </div>
                    <div class="post-status col-12 col-medium-2">
                        Status
                    </div>
                </div>
            </header>

            <?php foreach ( $pages as $page ): ?>
            <div class="post content padding-top-1 padding-bottom-1">
                <div class="row">
                    <div class="strong post-title col-12 col-medium-8">
                        <?php if ( get('cms.list.type') == 'global' ): ?>
                            <?php echo $page->slug; ?>
                        <?php else: ?>
                            <a class="cck-truncate" href="/admin/edit?id=<?php echo $page->id; ?>"><?php echo $page->title; ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="post-date col-12 col-medium-2">
                            <span class="cck-truncate tooltip" title="Last modified"><i class="material-icons margin-right-small">query_builder</i> <span class="timestamp" title="<?php echo $page->date; ?>"><?php echo date('d.m.Y', strtotime($page->date)); ?></span></span>
                    </div>
                    <div class="post-status col-12 col-medium-2">
                        <?php if ( $page->status === '1' ): ?>
                            <span class="color-positive published">Published</span>
                        <?php elseif ( $page->status === '0' ): ?>
                            <span class="color-grey published">Unpublished</span>
                        <?php else: ?>
                            <span class="color-negative published">Deleted</span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row post-actions">
                    <div class="col-12">
                        <div>
                            <a href="/admin/edit?id=<?php echo $page->id; ?>"><i class="material-icons margin-right-small">create</i> Edit</a>
                            <a href="/admin/delete?id=<?php echo $page->id; ?>" class="color-negative"><i class="material-icons margin-right-small">delete</i> Delete</a>

                            <?php if ( get('cms.list.type') == 'global' ): ?>
                                <span class="droplet-info"><strong>Info:</strong> Type <code><?php echo $page->type; ?></code></span>
                            <?php else: ?>
                                <span class="droplet-info"><strong>Info:</strong> Slug <code><?php echo $page->slug; ?></code></span>
                            <?php endif; ?>

                            <span class="droplet-info">ID <code><?php echo $page->id; ?></code></span>

                            <?php if ( $page->slug == 'index' ): ?><span class="droplet-info label">Homepage</span><?php endif; ?>
                            <?php do_trigger( 'cms_list_actions', $page ); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        </div>
        <p><a class="small positive button" href="/admin/new?post_type=<?php echo $type; ?>"><i class="material-icons margin-right-small">add</i> New <?php echo get('cms.types.'. $type .'.label'); ?></a></p>
        <?php else: ?>
        <div class="card">
            <div class="strong content padding-top-2 padding-bottom-2">
                <p>There are no <?php echo get('cms.list.title'); ?> to show.</p>
                <p><a class="small positive button" href="/admin/new?post_type=<?php echo $type; ?>"><i class="material-icons margin-right-small">add</i> New <?php echo get('cms.types.'. $type .'.label'); ?></a></p>
            </div>
        </div>
        <?php endif; ?>

    </div>

<?php get_footer(); ?>
