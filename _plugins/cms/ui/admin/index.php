<?php

set('page.title', get('site.company') . ' CMS');

get_header(); ?>

    <div class="container">

        <section class="page-title">
            <h1>Dashboard</h1>
            <h4 class="sub-heading">An overview of your site</h4>
        </section>

        <section class="dashboard-content">

            <?php get_partial('messages'); ?>

            <?php if ( $widgets = get('cms.widgets') ): ?>
            <div class="row">

                <?php foreach ( $widgets as $region => $content ): ?>
                    <div id="widgets-<?php echo $region; ?>" class="col-12 col-medium-4">

                        <?php foreach ( $content as $widget ): ?>
                            <div class="dashboard-card card">

                                <?php if ( array_key_exists( 'title', $widget['content'] ) ): ?>
                                <h3 class="title"><?php echo $widget['content']['title']; ?></h3>
                                <?php endif; ?>

                                <?php if ( array_key_exists( 'image', $widget['content'] ) ): ?>
                                <div class="image">
                                    <img src="<?php echo $widget['content']['image']; ?>">
                                </div>
                                <?php endif; ?>

                                <div class="content">
                                    <?php echo $widget['content']['content']; ?>
                                </div>

                                <?php if ( array_key_exists( 'footer', $widget['content'] ) ): ?>
                                <footer class="footer"><?php echo $widget['content']['footer']; ?></footer>
                                <?php endif; ?>

                            </div>
                        <?php endforeach; ?>

                    </div>
                <?php endforeach; ?>

            </div>
            <?php endif; ?>

        </section>

    </div>

<?php get_footer(); ?>
