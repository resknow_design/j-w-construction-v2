<?php

set('page.title', 'Users | ' . get('site.company') . ' CMS');

get_header(); ?>

    <div class="container">

        <section class="page-title">
            <h1><?php echo get('cms.list.title'); ?></h1>
        </section>

        <?php get_partial('messages'); ?>

        <?php if ( $users = get('cms.list.content') ): ?>
        <div class="post-list card">
            <header class="header">
                <div class="strong row">
                    <div class="post-title col-12 col-medium-8">
                        Username
                    </div>
                    <div class="post-date col-12 col-medium-2">
                        Date
                    </div>
                    <div class="post-status col-12 col-medium-2">
                        Status
                    </div>
                </div>
            </header>

            <?php foreach ( $users as $user ): ?>
            <div class="post content padding-top-1 padding-bottom-1">
                <div class="row">
                    <div class="strong post-title col-12 col-medium-8">
                        <a class="cck-truncate" href="/admin/user/edit?id=<?php echo $user->id; ?>"><?php echo $user->username; ?></a>
                    </div>
                    <div class="post-date col-12 col-medium-2">
                            <span class="cck-truncate tooltip" title="Last modified"><i class="material-icons margin-right-small">query_builder</i> <span class="timestamp" title="<?php echo $user->date; ?>"><?php echo date('d.m.Y', strtotime($user->date)); ?></span></span>
                    </div>
                    <div class="post-status col-12 col-medium-2">
                        <?php if ( $user->status === '1' ): ?>
                            <span class="color-positive published">Active</span>
                        <?php elseif ( $user->status === '2' ): ?>
                            <span class="color-positive published">New</span>
                        <?php elseif ( $user->status === '0' ): ?>
                            <span class="color-grey published">Inactive</span>
                        <?php else: ?>
                            <span class="color-negative published">Deleted</span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row post-actions">
                    <div class="col-12">
                        <div>
                            <a href="/admin/user/edit?id=<?php echo $user->id; ?>"><i class="material-icons margin-right-small">create</i> Edit</a>
                            <?php /* Hidden until 1.1.0 release <a href="/admin/user/delete?id=<?php echo $user->id; ?>" class="color-negative"><i class="material-icons margin-right-small">delete</i> Delete</a> */ ?>
                            <span class="droplet-info">ID <code><?php echo $user->id; ?></code></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        </div>
        <p><a class="small positive button" href="/admin/user/new"><i class="material-icons margin-right-small">add</i> New User</a></p>
        <?php else: ?>
        <div class="card">
            <div class="strong content padding-top-2 padding-bottom-2">
                <p>There are no <?php echo get('cms.list.title'); ?> to show.</p>
                <p><a class="small positive button" href="/admin/user/new"><i class="material-icons margin-right-small">add</i> New User</a></p>
            </div>
        </div>
        <?php endif; ?>

    </div>

<?php get_footer(); ?>
