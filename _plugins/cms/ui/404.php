<?php

set('page.title', 'Not Found | ' . get('site.company') . ' CMS');

get_header(); ?>

    <div class="container">

        <section class="page-title">
            <h1>404.</h1>
            <h4 class="sub-heading">We couldn't find that page.</h4>
        </section>

    </div>

<?php get_footer(); ?>
