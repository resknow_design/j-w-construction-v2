var gulp            = require('gulp'),
    sass            = require('gulp-ruby-sass'),
    cssnano         = require('gulp-cssnano'),
    uglify          = require('gulp-uglify'),
    rename          = require('gulp-rename'),
    concat          = require('gulp-concat'),
    notify          = require('gulp-notify'),
    plumber         = require('gulp-plumber'),
    del             = require('del'),
    autoprefixer    = require('gulp-autoprefixer'),
    imagemin        = require('gulp-imagemin');

// Style Tasks
gulp.task('styles', function() {
    return sass('assets/sass/style.scss', { style: 'expanded' })
    .pipe(plumber())
    .pipe(gulp.dest('assets/css'))
    .pipe(autoprefixer({cascade: false}))
    .pipe(cssnano())
    .pipe(gulp.dest('assets/css'))
    .pipe(notify({ message: 'CSS compiled' }));
});

// Javascript Tasks
gulp.task('scripts', function() {
    return gulp.src(['assets/js/source/*.js', 'assets/js/source/vendor/*.js'], { base: 'assets/js' })
    .pipe(plumber())
    .pipe(concat('main.js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
    .pipe(notify({ message: 'Javascript compiled' }));
});

// Optimize Images
// Not run during watch as it doesn't play nice.
gulp.task('images', function() {
    return gulp.src('assets/images/*')
    .pipe(imagemin({
        progressive: true,
        optimizationLevel: 7,
        interlaced: true,
        multipass: true
    }))
    .pipe(gulp.dest('assets/images'))
    .pipe(notify({ message: 'Images optimised' }));
});

// Housekeeping
gulp.task('clean', function(cb) {
    del(['assets/css'], cb)
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts');
});

// Watch
gulp.task('watch', function() {

    // Watch .scss files
    gulp.watch(['assets/sass/*.scss', 'assets/sass/**/*.scss'], ['styles']);

    // Watch .js files
    gulp.watch(['assets/js/source/*.js', 'assets/js/source/**/*.js'], ['scripts']);

});
