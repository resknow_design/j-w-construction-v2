$(document).ready(function() {


         //** ---------------------------------- **//
         // Repeater Field Functions
         //** ---------------------------------- **//

         /**
          * Loop through each repeat field
          * appending the necassary controls
          * to each field within.
          */
         $('.field-repeater .sub-field-actions').each(function() {

             // Append plus button
             $(this).append('<button class="primary small button repeater-add-field" type="button">+ Add row</button>');

         });

         /**
          * Loop through each field group
          * appending remove control.
          */
         $('.field-repeater .sub-field-group').each(function() {

             // Append plus button
             $(this).append('<button class="negative small button repeater-remove-field" type="button">&times;</button>');

         });

         /**
          * Add another field to the repeater
          * when clicking on the + button
          */
         $(document).on('click', '.repeater-add-field', function(e) {
             e.preventDefault();
             var field, newField;

             // Get the field HTML
             field = $(this).parent().parent().find('.sub-fields .sub-field-group').first().html();

             // Append it after the current field
             newField = $(this).parent().parent().find('.sub-fields').append('<div class="sub-field-group">' + field + '</div>');

         });

         /**
          * Remove a field from the repeater
          */
         $(document).on('click', '.repeater-remove-field', function(e) {
             e.preventDefault();
             var field;

             field = $(this).parent();

             if ( $(field).is(':only-child') && !$(field).next().hasClass('is-repeater') ) {
                 return false;
             }

             $(this).parent().fadeOut(function() {
                 $(this).remove();
             });
         });

});
