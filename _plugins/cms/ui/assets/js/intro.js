document.addEventListener('DOMContentLoaded', function() {

    var intro;

    // Get Intro JS object
    intro = introJs();

    // Start
    intro.start();

    // Dismiss all intros
    intro.oncomplete(function() {

        // Update user status
        $.get('/cms-ajax/user-activate', { status: 1 }, function(response) {
            //console.log(response);
        });

    });

});
