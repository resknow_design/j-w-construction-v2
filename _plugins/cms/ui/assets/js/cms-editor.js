$(document).ready(function() {

    // CMS Alerts
    function cmsAlert(type, msg, icon) {

        // Create alert
        if (icon) {
            $('body').append('<div class="cms-editor-alert alert-'+ type +'"><i class="material-icons">'+ icon +'</i>'+ msg +'</div>');
        } else {
            $('body').append('<div class="cms-editor-alert alert-'+ type +'">'+ msg +'</div>');
        }

        // Remove it after 5 seconds
        setTimeout(function() {
            $(document).find('.cms-editor-alert').fadeOut(function() {
                $(this).remove();
            })
        }, 5000);

    }

    // CMS Busy
    function cmsBusy(busy) {
        if (busy) {
            $('body').append('<div class="cms-editor-busy"></div>');
        } else {
            $(document).find('.cms-editor-busy').fadeOut(function() {
                $(this).remove();
            });
        }
    }

    // Launch the editor
    var editor;

    // Register Editor Styles
    ContentTools.StylePalette.add([
        new ContentTools.Style('Leading text (large font)', 'lead', ['p']),
        new ContentTools.Style('Quote', 'quote', ['p'])
    ]);

    // Get Editable Regions
    editor = ContentTools.EditorApp.get();
    editor.init('*[data-editable]', 'data-name');

    // Handle save
    editor.bind('save', function(regions) {
        var name, slug, payload, xhr;

        // Editor is busy whilst save is processing
        this.busy(true);
        cmsBusy(true);

        // Get the contents of each region
        payload = new FormData();
        for (name in regions) {
            if (regions.hasOwnProperty(name)) {
                payload.append(name, regions[name]);
            }
        }

        // Get page slug
        payload.append('slug', window.location.pathname);

        // Send the updates to the server
        function onStateChange(ev) {

            // Check request has finished
            if (ev.target.readyState == 4) {
                editor.busy(false);
                cmsBusy(false);

                console.log(ev.target.responseText);

                // Parse JSON
                var response = JSON.parse(ev.target.response);

                // Save was successful
                if (ev.target.status == 200) {
                    cmsAlert('positive', 'Your changes have been saved.', 'done');
                    console.log(response);
                } else {
                    cmsAlert('negative', 'An error occured, your changed could not be saved.', 'error');
                    console.log(response);
                }
            }
        }

        xhr = new XMLHttpRequest();
        xhr.addEventListener('readystatechange', onStateChange);
        xhr.open('POST', '/cms-ajax/save');
        xhr.send(payload);

    });

});
