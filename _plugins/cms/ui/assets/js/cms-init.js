$(document).ready(function() {

    // Initialise all editable elements
    $('.is-editable').each(function() {
        $(this).attr('data-editable', 'true').attr('data-name', $(this).attr('id'));
    });

    // Inject the toolbar
    $('body').append('<div id="cms-toolbar"></div>');
    $(document).find('#cms-toolbar').load('/cms-ajax/toolbar');

});
