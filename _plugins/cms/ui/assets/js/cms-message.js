$(document).ready(function() {

     /**
      * Dismiss Message
      *
      * Removes a message from the get_message array
      */
     $(document).on('click', '.cms-message a.close', function(e) {
         e.preventDefault();
         var id;

         // Set ID
         id = $(this).data('id');

         // Remove the message
         $.get('/cms-ajax/dismiss-message', { id: id }, function(response) {
             if ( response.code == 200 ) {
                 window.location.reload();
             }
         });
     });

});
