$(document).ready(function() {

    /**
     * Trigger Media Upload
     */
    $('[data-upload]').on('click', function(e) {
        e.preventDefault();
        var target = $(this).data('upload');
        $(target).trigger('click');
    });

    /**
     * Media Upload
     */
    $('.media-upload').each(function() {
        $(this).imageUpload({
            form: '#editor-form',
            handler: '/cms-ajax/upload',

            // onInit Callback - fired on plugin initilisation
            onInit: function() {
                //console.log('Plugin initialised');
            },

            // onSuccess Callback - fired on successful response from AJAX request
            onSuccess: function() {
                //console.log($(this));

                var theImage = $(this)[0].url;
                var container = $(this)[0].container;
                $(container).css('display', 'none');
                $(container).parent().find('.image').append('<img src="'+ theImage +'" />');
                $(container).parent().find('.image').css('display', 'block');
                $(container).find('.media-url').val(theImage);
                $(container).parent().find('.remove-image').css('display', 'block');
            },

            // onFail Callback - fired on unsuccessful response from AJAX request
            onFail: function() {
                //console.log($(this));
            },

            // onError Callback - fired when there was an error sending the AJAX request
            onError: function() {
                //console.log('It broke.');
            }
        });
    });

    /**
     * Remove Media Upload
     */
    $(document).on('click', '.image button.remove-image', function(e) {
        e.preventDefault();

        // Set Vars
        var field, parent;

        // Delete the field
        field = $(this).data('field-id');
        $.post('/cms-ajax/delete-field', { id: field }, function(response) {
            //console.log(response);
        });

        // Remove from UI
        parent = $(this).parent();
        $(parent).parent().find('.media-url').val('');
        $(parent).find('img').remove();

        // Restore placeholder
        $(parent).parent().find('.placeholder').css('display', 'block');
        $(this).css('display', 'none');

    });

});
