$(document).ready(function() {

    // Sidebar Toggle
    $('.sidebar-toggle').on('click', function(e) {
        e.preventDefault();
        $('.sidebar').toggleClass('is-open');
    });
    
});
