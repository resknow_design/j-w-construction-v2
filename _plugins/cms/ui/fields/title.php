<?php

/**
 * Title Field
 *
 * @TODO
 * - Add validation so that no more than
 *   1 instance can be added per post type.
 * - Write docs
 */

?>
<section class="field field-<?php echo $key; ?>"<?php if ( get('cms.new_user') ): ?> data-intro="Enter your post title here..." data-step="2"<?php endif; ?>>
    <input type="text" name="<?php echo $key; ?>" placeholder="<?php echo $field['label']; ?>"<?php if ( isset($content) && $content->$key ): ?> value="<?php echo $content->$key; ?>"<?php endif; ?> />
</section>
