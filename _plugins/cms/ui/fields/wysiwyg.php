<?php

/**
 * Wysiwyg Field
 *
 * @TODO
 * - Test single instance
 * - Test multiple instance
 * - Test within repeater
 * - Write docs
 */

?>
<?php if ( $key !== 'content' ): ?>
    <div class="card">
        <?php if ( !isset($field['hide_label']) ): ?>
        <h3 class="title"><?php echo $field['label']; ?></h3>
        <?php endif; ?>
        <div class="content">
        <?php endif; ?>

        <section class="field field-<?php echo $key; ?>"<?php if ( get('cms.new_user') ): ?> data-intro="Focus your cursor in this box to start writing your post. Click on the blue plus icon to add images to your post." data-step="3"<?php endif; ?>>
            <section class="wysiwyg<?php if ( $key !== 'content' ): ?> std-editor<?php endif; ?>">
                <div id="<?php echo $key; ?>" class="editor" data-key="<?php echo $key; ?>">
                    <?php

                    # Get Content
                    if ( isset($content) && $content->$key ) {
                        echo $content->$key;
                    }

                    ?>
                </div>
            </section>
        </section>

        <?php if ( $key !== 'content' ): ?>
        </div>
    </div>
<?php endif; ?>
