<?php

/**
 * Checkbox Field
 *
 * @TODO
 * - Test single instance
 * - Test multiple instance
 * - Test within repeater
 * - Write docs
 */

?>
<section class="field field-<?php echo $key; ?>">
    <?php echo $field['label']; ?>
    <?php foreach ( $field['options'] as $opt ): ?>
    <label>
        <input
            type="checkbox"
            name="<?php if ( isset($is_repeater) && $is_repeater == true ): echo $parent_key .'_'; endif; echo $key; ?>[]"
            value="<?php echo $opt['value']; ?>"
        />
        <?php echo $opt['label']; ?>
    </label>
    <?php endforeach; ?>
</section>
