<?php

/**
 * Colour Field
 *
 * @TODO
 * - Test multiple instance
 * - Test within repeater
 * - Write docs
 */

?>
<section class="field field-<?php echo $key; ?>">
    <?php if ( !isset($field['hide_label']) ): ?>
        <label for="<?php echo $key; ?>"><?php echo $field['label']; ?></label>
    <?php endif; ?>
    <input
        type="color"
        name="<?php if ( isset($is_repeater) && $is_repeater == true ): echo $parent_key .'_'; endif; echo $key; if ( isset($is_repeater) && $is_repeater == true ): ?>[]<?php endif; ?>"
        placeholder="<?php echo $field['label']; ?>"
    />
</section>
