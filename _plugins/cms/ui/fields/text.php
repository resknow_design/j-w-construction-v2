<?php

/**
 * Checkbox Field
 *
 * @TODO
 * - Test single instance
 * - Test multiple instance
 * - Test within repeater
 * - Write docs
 */

?>
<section class="field field-<?php echo $key; ?>">
    <?php if ( !isset($field['hide_label']) ): ?>
        <label for="<?php echo $key; ?>"><?php echo $field['label']; ?></label>
    <?php endif; ?>
    <input
        type="text"
        name="<?php if ( isset($is_repeater) && $is_repeater == true ): echo $parent_key .'_'; endif; echo $key; if ( isset($is_repeater) && $is_repeater == true ): ?>[]<?php endif; ?>"
        <?php if ( isset($field['placeholder']) ): ?>placeholder="<?php echo $field['placeholder']; ?>"<?php endif; ?>
    />
</section>
