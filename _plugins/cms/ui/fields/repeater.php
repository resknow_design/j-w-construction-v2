<?php

/**
 * Save this fields array
 * into a seperate variable
 * as the scope of $field will
 * change during the foreach loop
 */
$this_field = $field;

?>

<section class="field field-repeater field-<?php echo $key; ?>">
    <?php if ( isset($this_field['region']) && $this_field['region'] == 'editor' ): ?>
    <div class="card">
        <div class="content">
    <?php endif; ?>

        <?php if ( !isset($this_field['hide_label']) ): ?>
            <label for="<?php echo $key; ?>"><?php echo $this_field['label']; ?></label>
        <?php endif; ?>
        <div class="sub-fields">
            <?php

            /**
             * Set Parent Key to avoid naming
             * conflicts with other repeaters
             * and non-repeater fields
             */
            $parent_key = $key;

            /**
             * Set the item iteration so we can
             * wrap the first and last in a
             * .sub-field-group class
             */
            $i = 0;
            $c = count($this_field['sub_fields'])-1;

            /**
             * Loop through the sub fields
             */
            foreach ( $this_field['sub_fields'] as $key => $field ):

            /**
             * Set Repeater to true so we
             * know to collect the data in an
             * array.
             */
            $is_repeater = true;

            /**
             * Add start of wrapper for first field
             */
            if ( $i === 0 ):

            ?>
                <div class="sub-field-group"><?php endif; ?>
                    <div class="sub-field">
                        <?php include plugin_dir() . '/cms/ui/fields/' . $field['type'] . '.php'; ?>
                    </div>
                <?php if ( $i == $c ): ?></div>
            <?php endif; $i++; endforeach; ?>
        </div>
        <div class="sub-field-actions"></div>

    <?php if ( isset($this_field['region']) && $this_field['region'] == 'editor' ): ?>
        </div>
    </div>
    <?php endif; ?>

</section>
