<?php

/**
 * Textarea Field
 *
 * @TODO
 * - Test single instance
 * - Test multiple instance
 * - Test within repeater
 * - Write docs
 */

?>
<?php if ( isset($field['card']) ): ?>
<div class="card">
    <?php if ( !isset($field['hide_label']) ): ?>
    <h3 class="title"><?php echo $field['label']; ?></h3>
    <?php endif; ?>
    <div class="content">
        <section class="field field-<?php echo $key; ?>">
            <textarea
                name="<?php echo $key; ?>"
                <?php if ( isset($field['placeholder']) ): ?>placeholder="<?php echo $field['placeholder']; ?>"<?php endif; ?>
            ><?php /* Get Content */ if ( isset($content) && $content->$key ) { echo $content->$key; } ?></textarea>
            <?php if ( isset($field['description']) ): ?>
                <p class="description"><?php echo $field['description']; ?></p>
            <?php endif; ?>
        </section>
    </div>
</div>
<?php else: ?>
<section class="field field-<?php echo $key; ?>">
    <?php if ( !isset($field['hide_label']) ): ?>
        <label for="<?php echo $key; ?>"><?php echo $field['label']; ?></label>
    <?php endif; ?>
    <textarea
        name="<?php echo $key; ?>"
        <?php if ( isset($field['placeholder']) ): ?>placeholder="<?php echo $field['placeholder']; ?>"<?php endif; ?>
    ><?php /* Get Content */ if ( isset($content) && $content->$key ) { echo $content->$key; } ?></textarea>
    <?php if ( isset($field['description']) ): ?>
        <p class="description"><?php echo $field['description']; ?></p>
    <?php endif; ?>
</section>
<?php endif; ?>
