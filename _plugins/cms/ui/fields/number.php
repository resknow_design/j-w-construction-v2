<?php

/**
 * Numebr Field
 *
 * @TODO
 * - Add repeater markup
 * - Add "Editor" region markup
 * - Test single instance
 * - Test multiple instance
 * - Test within repeater
 * - Write docs
 */

?>
<section class="field field-<?php echo $key; ?>">
    <input type="number" name="<?php echo $key; ?>" placeholder="<?php echo $field['label']; ?>" />
</section>
