<?php

/**
 * Cover Image Field
 *
 * @TODO
 * - Add validation check to ensure that
 *   no more than 1 instance can be added
 *   to a single post type.
 * - Write docs
 */

?>
<section class="cover-image"<?php if ( get('cms.new_user') ): ?> data-intro="You can upload a Cover Image for your post too." data-step="4"<?php endif; ?>>
    <div class="placeholder" <?php if ( isset($content) && !empty($content->$key->value) && !$content->$key->is_deleted() ): ?>style="display: none;"<?php endif; ?>>
        <i class="material-icons">image</i>
        <button
            id="action-upload-cover"
            class="white button action-upload-cover"
            type="button"
            data-upload="#<?php echo $key; ?>_file">
            <i class="material-icons margin-right-small">add</i>
            <?php

            if ( isset($field['placeholder']) ) {
                echo $field['placeholder'];
            } else {
                echo $field['label'];
            }

            ?>
        </button>
        <input
            type="hidden"
            name="<?php if ( isset($is_repeater) && $is_repeater == true ): echo $parent_key .'_'; endif; echo $key; if ( isset($is_repeater) && $is_repeater == true ): ?>[]<?php endif; ?>"
            class="media-url"
            <?php if ( isset($content) && !empty($content->$key->value) && !$content->$key->is_deleted() ): ?>value="<?php echo $content->$key; ?>"<?php endif; ?>
        />
        <input
            type="file"
            name="<?php if ( isset($is_repeater) && $is_repeater == true ): echo $parent_key .'_'; endif; echo $key; ?>_image"
            id="<?php if ( isset($is_repeater) && $is_repeater == true ): echo $parent_key .'_'; endif; echo $key; ?>_file"
            class="media-upload"
        />
    </div>
    <div class="image" <?php if ( isset($content) && !empty($content->$key->value) && !$content->$key->is_deleted() ): ?>style="display: block;"<?php else: ?>style="display: none;"<?php endif; ?>>
        <?php if ( isset($content) && !empty($content->$key->value) && !$content->$key->is_deleted() ): ?><img src="<?php echo $content->$key; ?>"><?php endif; ?>
        <button class="remove-image small negative button" <?php if ( isset($content) && !empty($content->$key->value) && !$content->$key->is_deleted() ): ?>style="display: block;" data-field-id="<?php echo $content->$key->id; ?>"<?php endif; ?>>
            <i class="material-icons margin-right-small">delete</i> Remove image
        </button>
    </div>
</section>
