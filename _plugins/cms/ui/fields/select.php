<?php

/**
 * Select Field
 *
 * @TODO
 * - Test single instance
 * - Test multiple instance
 * - Test within repeater
 * - Write docs
 */

?>
<section class="field field-<?php echo $key; ?>">
    <?php if ( !isset($field['hide_label']) ): ?>
        <label for="<?php echo $key; ?>"><?php echo $field['label']; ?></label>
    <?php endif; ?>
    <select name="<?php echo $key; ?>">
        <?php foreach ( $field['options'] as $opt ): ?>
        <option value="<?php echo $opt['value']; ?>"><?php echo $opt['label']; ?></option>
        <?php endforeach; ?>
    </select>
    <?php if ( isset($field['description']) ): ?>
        <p class="description"><?php echo $field['description']; ?></p>
    <?php endif; ?>
</section>
