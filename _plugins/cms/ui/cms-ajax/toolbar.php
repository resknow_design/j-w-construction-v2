<header class="cms-toolbar">

    <nav class="navbar">
        <ul>
            <li>
                <a href="/admin/notifications" title="Notifications"><i class="material-icons">chat</i></a>
            </li>
            <li>
                <a href="#"><i class="material-icons">person</i></a>
                <ul class="cms-toolbar-dropdown">
                    <li><a href="/admin"><i class="material-icons">home</i> Dashboard</a></li>
                    <li><a href="/admin/logout"><i class="material-icons">block</i> Sign out</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</header>
