<?php get_partial('head'); ?>

    <?php if ( !is_path('admin/new') && !is_path('admin/edit') ): ?>
    <a class="sidebar-toggle" href="#"><i class="material-icons">menu</i></a>

    <nav class="sidebar">
        <header>
            <h2 class="cck-truncate">
                <a href="/admin">
                    <?php if ( $logo = get('cms.config.logo') ): ?>
                        <span class="site-logo" style="background-image: url('<?php echo $logo; ?>')"></span>
                    <?php endif; ?>
                    <?php echo get('site.company'); ?>
                </a>
            </h2>
            <a class="view-site" href="<?php echo get('site.url'); ?>" target="_blank" title="View your site"><i class="material-icons">visibility</i></a>
        </header>

        <section class="actions">
            <a class="positive button" href="/admin/new">New Post</a>
        </section>

        <ul>
            <li><a<?php if ( is_path('admin') ): ?> class="active"<?php endif; ?> href="/admin"><i class="material-icons">home</i> Dashboard</a></li>

            <?php if ( $types = get('cms.types') ): foreach( $types as $key => $type ): ?>
                <li><a<?php if ( is_path('admin/list/' . $key) ): ?> class="active"<?php endif; ?> href="/admin/list/<?php echo $key; ?>"><i class="material-icons"><?php echo $type['icon']; ?></i> <?php echo $type['plural']; ?></a></li>
            <?php endforeach; endif; ?>

            <?php
            /** Media Library & Settings to be added in a future release
            <li><a<?php if ( is_path('admin/media') ): ?> class="active"<?php endif; ?> href="/admin/media"><i class="material-icons">subscriptions</i> Media</a></li>
            <li><a<?php if ( is_path('admin/settings') ): ?> class="active"<?php endif; ?> href="/admin/settings"><i class="material-icons">settings</i> Settings</a></li>
            */
            ?>

            <?php if ( user_can('edit_user') ): ?>
            <li><a class="logout" href="/admin/users"><i class="material-icons">face</i> Users</a></li>
            <?php endif; ?>

            <li><a class="logout" href="/admin/help"><i class="material-icons">help</i> Help</a></li>

            <?php if ( user_can('edit_user') ): ?>
            <li><a href="/_plugins/cms/update.php"><i class="material-icons">autorenew</i> Updates</a></li>
            <?php endif; ?>

            <li><a href="/admin/logout"><i class="material-icons">exit_to_app</i> Logout</a></li>
        </ul>
    </nav>
    <?php endif; ?>
