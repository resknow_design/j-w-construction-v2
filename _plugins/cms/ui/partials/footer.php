<!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<?php if ( is_path('admin/new') || is_path('admin/edit') ): ?>
<script src="/_plugins/cms/ui/assets/vendor/alloy-editor/alloy-editor-all-min.js"></script>
<script src="/_plugins/cms/ui/assets/js/jquery.image-upload.js"></script>
<script src="/_plugins/cms/ui/assets/js/wysiwyg-editor.js"></script>
<script src="/_plugins/cms/ui/assets/js/editor-media-upload.js"></script>
<script src="/_plugins/cms/ui/assets/js/editor-repeater-functions.js"></script>
<?php endif; ?>

<?php if ( is_path('admin/login') || is_path('admin/password') || is_path('admin/user/new') || is_path('admin/user/edit') ): ?>
<script src="/_plugins/cms/ui/assets/js/cms-login.js"></script>
<?php endif; ?>

<?php if ( is_path('admin/help') ): ?>
<script src="/_plugins/cms/ui/assets/js/help.js"></script>
<?php endif; ?>

<?php if ( get('cms.new_user') ): ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/intro.js/2.0.0/intro.min.js"></script>
<?php do_trigger('cms_new_user_intro'); ?>
<?php endif; ?>

<script src="/_plugins/cms/ui/assets/js/sidebar-toggle.js"></script>
<script src="/_plugins/cms/ui/assets/js/cms-message.js"></script>

</body>
</html>
