<?php if ( $messages = get_messages() ): ?>
<div class="cms-messages">
    <?php foreach ( $messages as $id => $message ): ?>
    <div class="cms-message alert <?php echo $message['type']; ?>">
        <?php echo $message['message']; ?>
        <a class="close" data-id="<?php echo $id; ?>" href="#"></a>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>
