<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
    <link rel="dns-prefetch" href="//fonts.googleapis.com">

    <title><?php echo get('page.title'); ?></title>

    <?php if ( is_path('admin/new') || is_path('admin/edit') ): ?>
    <link rel="stylesheet" href="/_plugins/cms/ui/assets/vendor/alloy-editor/assets/alloy-editor-ocean.css">
    <link rel="stylesheet" href="/_plugins/cms/ui/assets/css/jquery.image-upload.css">
    <?php endif; ?>

    <?php if ( get('cms.new_user') ): ?>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intro.js/2.0.0/introjs.min.css">
    <?php endif; ?>

    <link rel="stylesheet" href="/_plugins/cms/ui/assets/css/style.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Merriweather:400,700|Open+Sans:300,400,600,700">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Touch icons & Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/_plugins/cms/ui/assets/images/icons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/_plugins/cms/ui/assets/images/icons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/_plugins/cms/ui/assets/images/icons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/_plugins/cms/ui/assets/images/icons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/_plugins/cms/ui/assets/images/icons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/x-icon" href="/_plugins/cms/ui/assets/images/icons/favicon.ico">

</head>
<body class="page-<?php echo get('page.slug'); ?>">
