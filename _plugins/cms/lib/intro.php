<?php

use CMS\CMS;

/**
 * Intro
 *
 * Show new users around the UI.
 */

// Get User
$cms = CMS::run();
$user = $cms->user();

if ( $user->is_new() ) {

    // Set New User Flag
    set('cms.new_user', true);

    // Echo introJs init call
    function intro_js() {
        echo '<script src="/_plugins/cms/ui/assets/js/intro.js"></script>';
    }

    // Add Action to trigger the function
    add_action( 'cms_new_user_intro', 'intro_js' );

}
