<?php

# Load Compat file
require_once plugin_dir() . '/cms/lib/compat.php';

# Load CMS class
require_once plugin_dir() . '/cms/classes/CMS/CMS.php';
require_once plugin_dir() . '/cms/classes/CMS/Util.php';
require_once plugin_dir() . '/cms/classes/CMS/User.php';
require_once plugin_dir() . '/cms/classes/CMS/Widgets.php';

# Check Medoo class exists
if ( !class_exists('medoo') ) {
    require_once plugin_dir() . '/cms/classes/vendor/medoo.php';
}

# Load Droplet & Field classes
require_once plugin_dir() . '/cms/classes/CMS/Collection.php';
require_once plugin_dir() . '/cms/classes/CMS/Droplet.php';
require_once plugin_dir() . '/cms/classes/CMS/Field.php';

# Get JSON Class
if ( !class_exists('JSON') ) {
    require_once plugin_dir() . '/cms/classes/vendor/JSON.php';
}

# Check GUMP is available
if ( !class_exists('GUMP') ) {
    require_once plugin_dir() . '/cms/classes/vendor/GUMP.php';
}

# Check Upload class is available
if ( !class_exists('Upload\Storage\Filesystem') ) {
    require_once plugin_dir() . '/cms/classes/vendor/Upload/Autoloader.php';
}

# Load Messages functions
require_once plugin_dir() . '/cms/functions/messages.php';

# Load Defaults
require_once plugin_dir() . '/cms/lib/defaults.php';
require_once plugin_dir() . '/cms/functions/user.php';

# Load Intro
require_once plugin_dir() . '/cms/lib/intro.php';
