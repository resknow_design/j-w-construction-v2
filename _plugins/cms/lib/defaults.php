<?php

use CMS\Widgets;

/**
 * Default Widgets
 */

# Recent Posts
function cms_widget_recent_posts() {

    # Load Recent Posts
    $posts = new CMS\Collection();
    $recent = $posts->get(array(
        'type'  => 'post',
        'limit' => 3,
        'status' => 1
    ));

    # Start output
    $output = '<ul class="basic">';

    # Check posts exists
    if ( !empty($recent) ) {
        foreach ( $recent as $post ) {
            $output .= '<li><a href="/admin/edit?id=' . $post->id . '">' . $post->title . '</a></li>';
        }
    } else {
        $output .= '<p>You haven\'t posted yet!</p><p><a class="small positive button" href="/admin/new">New Post</a></p>';
    }

    # Close output
    $output .= '</ul>';

    return $output;

}

# Add Recent Posts
$widgets = Widgets::run();
$widgets->add_widget( 'recent_posts', 'left', array(
    'title'         => 'Recent Posts',
    'content'       => cms_widget_recent_posts()
) );
