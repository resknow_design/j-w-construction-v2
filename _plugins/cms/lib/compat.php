<?php

// Use Controller
if ( !function_exists('use_controller') ) {

    /**
     * Use Controller
     *
     * @global $_controller
     * @param $dir (string) Directory of controller to use
     * @return void
     */
    function use_controller( $dir ) {
        global $_controller;
        return $_controller->use_controller($dir);
    }

}

// Hash Equals
if ( !function_exists('hash_equals') ) {

    /**
     * Hash Equals
     *
     * Emulates the hash_equals function in PHP if it's
     * not defined
     *
     * @param $str1 (string) First string to compare
     * @param $str2 (string) Second string to compare
     * @return (bool) True is both strings match
     */
    function hash_equals( $str1, $str2 ) {

        if ( strlen($str1) != strlen($str2) ) {
            return false;
        } else {
            $res = $str1 ^ $str2;
            $ret = 0;
            for ( $i = strlen($res) - 1; $i >= 0; $i-- ) $ret |= ord($res[$i]);
            return !$ret;
        }
        
    }

}
