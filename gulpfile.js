var gulp            = require('gulp'),
    sass            = require('gulp-ruby-sass'),
    minifycss       = require('gulp-minify-css'),
    uglify          = require('gulp-uglify'),
    rename          = require('gulp-rename'),
    concat          = require('gulp-concat'),
    notify          = require('gulp-notify'),
    plumber         = require('gulp-plumber'),
    del             = require('del'),
    autoprefixer    = require('gulp-autoprefixer'),
    merge           = require('merge2'),
    newer           = require('gulp-newer'),
    imagemin        = require('gulp-imagemin'),
    resize          = require('gulp-image-resize');

    // Config
    var config = {
        source: '_templates/assets',
        dest: '_templates/assets',
        imageSizes: [
            { width: 320 },
            { width: 420 },
            { width: 768 },
            { width: 1024 },
            { width: 1280 },
            { width: 1600 }
        ]
    }

    // Style Tasks
    gulp.task('styles', function() {
        return sass(config.source + '/sass/*.scss', { style: 'compressed' })
        .on('error', sass.logError)
        .pipe(plumber())
        .pipe(gulp.dest(config.dest + '/css'))
        .pipe(autoprefixer({cascade: false}))
        .pipe(minifycss())
        .pipe(gulp.dest(config.dest + '/css'))
        .pipe(notify({ message: 'Styles task complete' }));
    });

    // Javascript Tasks
    gulp.task('scripts', function() {
        return gulp.src([config.source + '/js/source/*.js', config.source + '/js/source/vendor/*.js'], { base: config.source + '/js/source' })
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(config.dest + '/js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.dest + '/js'))
        .pipe(notify({ message: 'Scripts task complete' }));
    });

    // Image Tasks
    gulp.task('image', () => {

        // Map each element to a Gulp Stream
        const streams = config.imageSizes.map(el => {
            return gulp.src([config.source + '/images/**/*'])
            .pipe(rename(file => file.basename += '-' + el.width)) // Append width to the filename
            .pipe(newer(config.dest + '/images')) // Check if the image has been changed
            .pipe(resize(el)) // Resize the image
            .pipe(imagemin()) // Opitmise the image
            .pipe(gulp.dest(config.dest + '/images'))
        });

        return merge(streams);

    });

    // Housekeeping
    gulp.task('clean', function(cb) {
        del([config.source + '/css'], cb)
    });

    // Default task
    gulp.task('default', ['clean'], function() {
        gulp.start('styles', 'scripts');
    });

    // Watch
    gulp.task('watch', function() {

        // Watch .scss files
        gulp.watch([config.source + '/sass/*.scss', config.source + '/sass/**/*.scss', config.source + '/sass/**/**/*.scss'], ['styles']);

        // Watch .js files
        gulp.watch([config.source + '/js/source/*.js', config.source + '/js/source/**/*.js'], ['scripts']);

        // Watch Images
        gulp.watch([config.source + '/images/**/*', config.source + '/images/*'], ['image']);

    });
