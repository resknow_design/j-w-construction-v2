<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Home | ' . get('site.company'));
set('page.description', 'A few words about this page should be here...');

add_script( 'gallery-init', '/_templates/assets/js/lightcase.js' );
add_stylesheet( 'gallery-style', '/_templates/assets/css/lightcase.css');

$gallery = glob("_templates/assets/images/gallery/paving-landscaping/*.{jpg,png,JPG}", GLOB_BRACE);

get_header(); ?>

<section class="container">
    <div class="grid wrapper">
        <div class="content col-whole md-col-half lg-col-two-third">
            <h1>Paving and Landscaping, Maidstone</h1>
            <p>
                Looking for advice with patios and driveways in Maidstone and all surrounding Boroughs and Counties?
            </p>
            <p>
                J &amp; W Construction have been vetted and monitored by Marshalls expert assessors to ensure our levels of competence and performance.
            </p>
            <p>
                All patio and driveway installations come with a 10 year Marshalls Hard Landscape Guarantee. The guarantee protects all home owners of their investment with a level of cover ummatched from any other company. This also covers material defects for a full 10 years, plus installation defects for 5 years.
            </p>
            <p>
                You can also find J &amp; W Construction by visiting www.marshalls.co.uk/homeowners
            </p>
            <p>
                <strong>We have previously completed work within:</strong>
            </p>
            <ul>
                <li>Driveways</li>
                <li>Block Paving</li>
                <li>Hard Landscaping</li>
                <li>Fencing</li>
                <li>Decking and Patio's</li>
                <li>Brickwork</li>
                <li>Artificial Lawns</li>
            </ul>
            <p>
                <strong>You can see some examples of our workmanship throughout the website and by clicking on the images below.</strong>
            </p>
            <div class="content">
                <?php if(!empty($gallery)): ?>
                    <?php foreach($gallery as $image): ?>

                        <div class="col-whole sm-col-third">
                            <a href="/<?php echo $image; ?>" data-rel="lightcase:myCollection" class="gallery-style" style="background-image: url('/<?php echo $image; ?>')"></a>
                        </div>

                    <?php endforeach;?>
                <?php endif; ?>
            </div>
        </div>
        <div class="content col-whole md-col-half lg-col-third">
            <?php echo get_partial('sidebar'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
