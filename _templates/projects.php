<?php

use CMS\Collection;
use CMS\Droplet;

/**
 * Set Page Meta Tags
 */
set('page.title', 'Home | ' . get('site.company'));
set('page.description', 'A few words about this page should be here...');

add_script( 'slick', assets_dir('/', false) . '/js/slick.js' );
add_stylesheet( 'slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css');
add_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js');

if ( get('page.index.1') ) {
    $post = new Droplet(get('page.index.1'));
} else {
    $collection = new Collection();
    $posts = $collection->get(array(
        'type' => 'post',
        'status' => 1
    ));
}

get_header(); ?>

<section class="container">
    <div class="grid wrapper">
        <div class="content col-whole md-col-half lg-col-two-third">
                <h1>Projects</h1>
                <?php if ( !isset($posts) ): ?>

                <?php

                // Default cover image
                if ( empty($post->cover_image->value) ) {
                    $image = assets_dir('/', false) . '/images/default.png';
                } else {
                    $image = $post->cover_image;
                }

                ?>
                <div class="project">
                    <div class="col-whole">
                        <img class="project-image-big" src="<?php echo $image; ?>">
                    </div>
                    <div class="col-whole">
                        <h2><?php echo $post->title; ?></h2>
                        <?php echo $post->content; ?>
                        <div class="back-button">
                            <p><a class="back" href="/projects">Back to Projects</a></p>
                        </div>
                    </div>
                </div>

                <?php else: ?>

                <?php foreach ( $posts as $post ): ?>

                <?php

                // Default cover image
                if ( empty($post->cover_image->value) ) {
                    $image = assets_dir('/', false) . '/images/default.png';
                } else {
                    $image = $post->cover_image;
                }

                ?>

                <div class="grid project">
                    <div class="content col-whole sm-col-half md-col-whole lg-col-third">
                        <img class="project-image" src="<?php echo $image; ?>">
                    </div>
                    <div class="col-whole sm-col-half md-col-whole lg-col-two-third">
                        <a href="/projects/<?php echo $post->slug; ?>"><h2><?php echo $post->title; // Title ?></h2></a>
                        <p class="snippet"><?php echo substr( strip_tags($post->content), 0, 150 ); ?> <a href="/projects/<?php echo $post->slug; ?>">Read more...</a></p>
                    </div>
                </div>


            <?php endforeach; endif; ?>
        </div>

        <div class="content col-whole md-col-half lg-col-third">
            <?php echo get_partial('sidebar'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
