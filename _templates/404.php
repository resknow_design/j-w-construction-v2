<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Page Not Found | ' . get('site.company'));
set('page.description', 'The page you were looking for could not be found.');

get_header(); ?>

<div class="container">
    <div class="grid wrapper">
        <h1>Page Not Found.</h1>
        <p>Oops! The page you were looking for could not be found! :(</p>
    </div>
</div>



<?php get_footer(); ?>
