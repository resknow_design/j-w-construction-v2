<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Home | ' . get('site.company'));
set('page.description', 'A few words about this page should be here...');

add_script( 'slick', assets_dir('/', false) . '/js/slick.js' );
add_stylesheet( 'slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css');
add_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js');

add_script( 'gallery-init', '/_templates/assets/js/lightcase.js' );
add_stylesheet( 'gallery-style', '/_templates/assets/css/lightcase.css');

$gallery = glob("_templates/assets/images/gallery/construction/*.{jpg,png,JPG}", GLOB_BRACE);

get_header(); ?>

<section class="container">
    <div class="grid wrapper">
        <div class="content col-whole md-col-half lg-col-two-third">
            <h1>Brickwork and Walling, Maidstone</h1>
            <p>
                J &amp; W Construction ensure that all brickwork, walling, foundations and any drainage carried out is completed to the highest level.
            </p>
            <p>
                <strong>Some examples of the construction and building services that we offer include:</strong>
            </p>
            <ul>
                <li>Brickwork</li>
                <li>Walling</li>
                <li>Drainage</li>
                <li>Foundations</li>
                <li>Autonative Rates</li>
                <li>Railings</li>
                <li>Lighting</li>
            </ul>
            <p>
                Our business also covers the installation of wrought iron gates and railings, all external lighting and any drainage from service to underground.
            </p>
            <div class="content">
                <?php if(!empty($gallery)): ?>
                    <?php foreach($gallery as $image): ?>

                        <div class="col-whole sm-col-third">
                            <a href="/<?php echo $image; ?>" data-rel="lightcase:myCollection" class="gallery-style" style="background-image: url('/<?php echo $image; ?>')"></a>
                        </div>

                    <?php endforeach;?>
                <?php endif; ?>
            </div>
        </div>
        <div class="content col-whole md-col-half lg-col-third">
            <?php echo get_partial('sidebar'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
