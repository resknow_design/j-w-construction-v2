<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Contact Us | ' . get('site.company'));
set('page.description', 'A few words about this page should be here...');

add_script( 'google-map-cdn', '//maps.googleapis.com/maps/api/js?key=AIzaSyAeP_G3ffklPHmkCIdhkVGq0GtzhusHHls' );
add_script( 'google-map', assets_dir('/', false) . '/js/map.js' );

get_header(); ?>

<!-- MAP

<div class="hero">
    <div id="map-canvas" class="map-canvas" data-lat="51.405104" data-lng="0.105223" alt="Map of Address"></div>
</div>

-->

<section="container">
    <div class="grid wrapper">
        <div class="content col-whole md-col-half lg-col-two-third">
            <h1>Find Us</h1>
            <div class="hero">
                <div id="map-canvas" class="map-canvas" data-lat="51.435238" data-lng="0.343555" alt="Map of Address"></div>
            </div>
        </div>
        <div class="content col-whole md-col-half lg-col-third">
            <?php echo get_partial('sidebar'); ?>
        </div>
    </div>
</section>

<!-- LOCAL BUSINESS

<section class="container">
    <div class="grid wrapper">
        <div class="content">
            <div itemscope itemtype="http://schema.org/LocalBusiness">
                 <span itemprop="telephone"><?php echo get('site.phone'); ?></span>

                 <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress"></span>
                    <span itemprop="addressRegion"></span>
                    <span itemprop="postalCode"></span>
                 </div>

                 <span itemprop="email"></span>
            </div>
        </div>
    </div>
</section>

-->

<?php get_footer(); ?>
