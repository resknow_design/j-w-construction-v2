<div class="footer">
    <div class="wrapper">
        <p>&copy; 2016 <?php echo get('site.company'); ?>, All rights reserved.<br>
            Web Designed, Owned &amp; Maintained by &nbsp;<img class="owner" src="http://assets.resknow.co.uk/branding/searchitlocal/icon-48x48-blue.png" alt="Search It Local"> Search It Local</p>
    </div>
</div>

<!--

##### RESKNOW #####
Web Design by &nbsp;<img class="owner" src="http://assets.resknow.co.uk/branding/resknow/r-48x48.png" alt="Resknow"> Resknow

##### SEARCH IT LOCAL #####
Web Designed, Owned & Maintained by &nbsp;<img class="owner" src="http://assets.resknow.co.uk/branding/searchitlocal/icon-48x48-blue.png" alt="Search It Local"> Search It Local

-->

<?php get_partial('analytics'); ?>

<!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<?php if ( get('site.scripts') ): ?>
    <?php foreach ( get('site.scripts') as $script ): ?>
        <script src="<?php echo $script; ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>

<?php if ( get('page.scripts') ): ?>
    <?php foreach ( get('page.scripts') as $script ): ?>
        <script src="<?php echo $script; ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>

<script>document.addEventListener("DOMContentLoaded",function(){cookieChoices.showCookieConsentBar("<?php echo get('site.company'); ?> uses cookies to give you the best experience.","OK","Learn more","http://www.allaboutcookies.org")});</script>

</body>
</html>
