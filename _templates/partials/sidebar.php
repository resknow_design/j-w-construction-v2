<div class="border-left">
    <h2 class="heading"><span class="highlight">Contact Us</span></h2>
    <div class="contactform">
        <?php echo get_form('quick'); ?>
    </div>
    <h2><span class="highlight">CONTACT DETAILS</span></h2>
    <p>
        Maidstone telephone: 01622 829 403
    </p>
    <p>
        Mobile: 07768 403520
    </p>
    <p>
        Free Phone: 0808 225 5515
    </p>
    <?php if (!is_home() ): ?>
        <h2><span class="highlight">APPROVED BY:</span></h2>
        <div class="content col-whole">
            <div class="image-container">
                <img src="<?php assets_dir(); ?>/images/marshallsregisterlogo.jpg" class="image">
            </div>
        </div>
    <?php endif; ?>
</div>
