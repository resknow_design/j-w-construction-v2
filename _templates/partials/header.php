<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">

    <title><?php echo get('page.title'); ?></title>
    <?php if ( get('page.description') ): ?><meta name="description" content="<?php echo get('page.description'); ?>"><?php endif; ?>
    <meta name="keywords" content="<?php if ( get('page.keywords') ): echo get('page.keywords'); else: echo get('site.keywords'); endif; ?>">
    <?php if ( get('page.canonical') ): ?><link rel="canonical" href="<?php echo get('page.canonical'); ?>"><?php endif; ?>

    <!-- Site wide stylesheets -->
    <?php if ( get('site.stylesheets') ): ?>
        <?php foreach ( get('site.stylesheets') as $stylesheet ): ?>
            <link rel="stylesheet" type="text/css" href="<?php echo $stylesheet; ?>">
        <?php endforeach; ?>
    <?php endif; ?>

    <!-- Page specific stylesheets -->
    <?php if ( get('page.stylesheets') ): ?>
        <?php foreach ( get('page.stylesheets') as $stylesheet ): ?>
            <link rel="stylesheet" type="text/css" href="<?php echo $stylesheet; ?>">
        <?php endforeach; ?>
    <?php endif; ?>

    <!-- Touch icons & Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php assets_dir(); ?>/images/icons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php assets_dir(); ?>/images/icons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php assets_dir(); ?>/images/icons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php assets_dir(); ?>/images/icons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php assets_dir(); ?>/images/icons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/x-icon" href="<?php assets_dir(); ?>/images/icons/favicon.ico">

</head>
<body class="page-<?php echo get('page.slug'); ?>" itemscope itemtype="http://schema.org/WebPage">

    <?php get_partial('browser-update'); ?>

<div class="contact-bar content">
    <div class="grid wrapper">
        <div class="padding col-two-third md-col-whole">
            <p>
                <span class="highlight">Free:</span> 0808 225 55 15
            </p>
            <p>
                <span class="highlight">Tel:</span> 01622 829 403
            </p>
        </div>
        <div class="col-third md-col-whole">
            <div class="menu">
                <button class="hamburger hamburger--spin toggle" type="button" id="navToggle" href="#">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="header">
    <div class="container grid wrapper">
        <div class="logo-container col-whole md-col-third">
            <img src="<?php assets_dir(); ?>/images/logo.jpg" alt="J&amp;W Construction Logo" class="logo">
        </div>
        <div class="nav col-whole md-col-two-third" id="nav">
            <ul>
                <li <?php if ( is_home() ): ?>class="active"<?php endif; ?>><a href="/">Home</a></li>
                <li class="work-hover"><a href="#">Work <i id="showMore" class="zmdi zmdi-caret-down"></i></a>
                    <ul id="showMoreList">
                        <li <?php if ( is_page('construction') ): ?>class="active"<?php endif; ?>><a href="/construction">Construction</a></li>
                        <li <?php if ( is_page('paving-and-landscaping') ): ?>class="active"<?php endif; ?>><a href="/paving-and-landscaping">Paving &amp; Landscaping</a></li>
                        <li <?php if ( is_page('warranty') ): ?>class="active"<?php endif; ?>><a href="/warranty">Warranty</a></li>
                    </ul>
                </li>
                <li <?php if ( is_page('projects') ): ?>class="active"<?php endif; ?>><a href="/projects">Projects</a></li>
                <li <?php if ( is_page('testimonials') ): ?>class="active"<?php endif; ?>><a href="/testimonials">Testimonials</a></li>
                <li <?php if ( is_page('contact') ): ?>class="active"<?php endif; ?>><a href="/contact">Contact</a></li>
            </ul>
        </div>


    </div>
</div>
