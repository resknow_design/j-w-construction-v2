document.addEventListener('DOMContentLoaded', function() {

    // Nav
    var menu, toggleNav, showMore;

    showMore = document.getElementById('showMore');
    menu = document.getElementById('nav');
    toggleNav = document.getElementById('navToggle');

    toggleNav.addEventListener('click', function (e) {
        e.preventDefault();
        menu.classList.toggle('isOpen');
        toggleNav.classList.toggle('is-active');
    });

    showMore.addEventListener('click', function (e) {
        e.preventDefault();
        showMoreList.classList.toggle('is-open');
        showMore.classList.toggle('zmdi-caret-down');
        showMore.classList.toggle('zmdi-caret-up');
    });

});
