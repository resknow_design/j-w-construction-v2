$(document).ready(function() {
    $('.page-banner').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 1500,
        fade: true,
        slide: '.slide',
        autoplay: true
    });
});
