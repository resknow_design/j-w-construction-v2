<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Home | ' . get('site.company'));
set('page.description', 'A few words about this page should be here...');

add_script( 'slick', assets_dir('/', false) . '/js/slick.js' );
add_stylesheet( 'slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css');
add_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js');

get_header(); ?>

<section class="page-banner">
    <div class="slide slide-one"></div>
    <div class="slide slide-two"></div>
</section>

<section class="container">
    <div class="grid wrapper">
        <div class="content col-whole md-col-half lg-col-two-third">
            <h1>J &amp; W Construction, Maidstone</h1>
            <h2>SPECIALISTS IN PAVING, DRIVEWAYS AND BRICKWORK</h2>
            <p>
                J &amp; W Construction is a family run business that has been providing high quality paving, landscaping and brickwork throughout Kent for 25 years. Our reputation is earned from our experience in the industry and our keen attention to detail. Our dedicated approach and our quality craftsmanship are what makes J &amp; W Construction different.
            </p>
            <p>
                J &amp; W Construction is a member of the Marshalls Register of Accredited Landscape Contractors and Driveway Installers. This means two things, the peace of mind for all houseowners knowing their employing proffessionals who can install products correctly and to high standards.
            </p>
            <div class="col-whole">
                <div class="col-whole md-col-half lg-col-two-third">
                    <p>
                        J &amp; W Construction can ensure you get the best possible results for your project from start to finish, giving you the peace of mind that you can only get from a quality registered Mashalls Installer.
                    </p>
                </div>

                <div class="col-whole md-col-half lg-col-third">
                    <div class="image-container">
                        <img src="<?php assets_dir(); ?>/images/marshallsregisterlogo.jpg" class="image">
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="col-whole md-col-whole lg-col-half">
                    <h2>COMPETENT SERVICE</h2>
                    <p>
                        We offer a wonderful combination of competitive prices, quality craftsmanship, friendly service and competent workers.
                    </p>
                    <p>
                        <strong>The work we undertake includes:</strong>
                    </p>
                    <ul>
                        <li>Patio's</li>
                        <li>Fencing</li>
                        <li>Driveways</li>
                        <li>Brickwork</li>
                        <li>Water features</li>
                        <li>Landscaping</li>
                        <li>Drainage</li>
                    </ul>
                    <p>
                        Whenever you need domestic builders, remember we are experienced and competent in our trade.
                    </p>
                </div>
                <div class="col-whole md-col-whole lg-col-half">
                    <h2>CALL US TODAY</h2>
                    <p>
                        If you are looking for an estimate on garden paths and driveways in Kent, we will be happy to give you a quotation.
                    </p>
                    <p>
                        <strong>We regularly work in all of the following areas:</strong>
                    </p>

                    <ul>
                        <li>Maidstone</li>
                        <li>Medway</li>
                        <li>Chatham</li>
                        <li>Tunbridge Wells</li>
                        <li>Sevenoaks</li>
                        <li>Ashford</li>
                    </ul>
                    <p>
                        We are certain that you will be delighted with our friendly service, quality workmanship and competitive prices, which have already been enjoyed by customers of J &amp; W Construction
                    </p>
                </div>
            </div>
        </div>
        <div class="content col-whole md-col-half lg-col-third">
            <?php echo get_partial('sidebar'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
