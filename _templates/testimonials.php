<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Home | ' . get('site.company'));
set('page.description', 'A few words about this page should be here...');

add_script( 'slick', assets_dir('/', false) . '/js/slick.js' );
add_stylesheet( 'slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css');
add_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js');

get_header(); ?>

<section class="container">
    <div class="grid wrapper">
        <div class="content col-whole md-col-half lg-col-two-third">
            <h1>Testimonials</h1>

            <div class="testimonials">
                <div class="testimonial">
                    <p class="name"> - G Coleman</p>
                    <p>
                        JWC did a first rate job at a fixed &amp; competitive price. The work involved digging up &amp; removing old concrete drive, removal of tree &amp; tree stump, widening the drive &amp; laying of block paviors.
                    </p>
                    <p>
                        All the preparation work was done to a high standard. The workforce was prompt &amp; courteous &amp; the whole job was thoroughly well managed. In addition to a high standard of workmanship JWC are able to offer a Marshalls guarantee for both product &amp; construction.
                    </p>
                    <p>
                        I have no hesitation in recommending JWC.
                    </p>
                </div>
                <div class="testimonial">
                    <p class="name"> - B and S Sakaria, Borden</p>
                    <p>
                        We used J&amp;W Construction for building our extension, including decorating and making good, completing with the patio and other appropriate work because we had been very impressed with the quality of their work, their commitment and professional approach when they had built our drive at an earlier time.
                    </p>
                    <p>
                        Both Wayne and John and all the team they employ were punctual, kept us informed about all the works being carried out, dealt with any complications calmly and solved problems very efficiently. They have an eye for detail and we found them to be very thorough. The standard of their work is excellent. They were amusing and very approachable as a bonus!
                    </p>
                    <p>
                        We recommend them highly.
                    </p>
                </div>
            </div>
        </div>
        <div class="content col-whole md-col-half lg-col-third">
            <?php echo get_partial('sidebar'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
