<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Home | ' . get('site.company'));
set('page.description', 'A few words about this page should be here...');

add_script( 'slick', assets_dir('/', false) . '/js/slick.js' );
add_stylesheet( 'slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css');
add_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js');

add_script( 'gallery-init', '/_templates/assets/js/lightcase.js' );
add_stylesheet( 'gallery-style', '/_templates/assets/css/lightcase.css');

$gallery = glob("_templates/assets/images/gallery/construction/*.{jpg,png,JPG}", GLOB_BRACE);

get_header(); ?>

<section class="container">
    <div class="grid wrapper">
        <div class="content col-whole md-col-half lg-col-two-third">
            <h1>Warranties</h1>
            <h2>THE MARSHALLS HARD LANDSCAPE GUARANTEE</h2>
            <p>
                Only a Marshalls Register Installer can offer you the unique Marshalls Hard Landscape Guarantee. The guarantee protects your investment with a level of cover unmatched by any other company:
            </p>
            <ul>
                <li>Covers material defects for a full 10 year</li>
                <li>Covers installation defects for 5 years</li>
                <li>Is valid even if the Installer ceases trading</li>
                <li>Is index-linked, to protect your investment against inflation</li>
                <li>Is issued only when inspected and signed as satisfactory by you, the customer</li>
                <li>Upon receipt of the correctly completed request form and fee, Marshalls will send your guarantee within 5 days</li>
                <li>Is great value at just £50 (+VAT) per £5,000 of the installed value</li>
            </ul>
            <p>
                <strong>Marshalls advise customers always to take out suitable protection against fault, such as The Hard Landscape Guarantee.*</strong>
            </p>
        </div>
        <div class="content col-whole md-col-half lg-col-third">
            <?php echo get_partial('sidebar'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
