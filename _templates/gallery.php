<?php

/**
 * Set Page Meta Tags
 */
set('page.title', 'Gallery | ' . get('site.company'));
set('page.description', 'Pet & Garden Supplies, Pet Toys, Pet Food, Pet Gifts, Dartford');

add_script( 'gallery-init', '/_templates/assets/js/lightcase.js' );
add_stylesheet( 'gallery-style', '/_templates/assets/css/lightcase.css');

$gallery = glob("_templates/assets/images/gallery/*.png");

get_header(); ?>

<div class="grid wrapper">

    <?php if(!empty($gallery)): ?>
        <?php foreach($gallery as $image): ?>

            <div class="col-whole col-quarter">
                <a href="/<?php echo $image; ?>" data-rel="lightcase:myCollection" class="gallery-style" style="background-image: url('/<?php echo $image; ?>')"></a>
            </div>

        <?php endforeach;?>
    <?php endif; ?>

</div>


<?php get_footer(); ?>
