<?php

/**
 * Extras Enabled
 *
 * @deprecated as of 1.4.1 - will always return true for
 * backwards compatibility with plugins using this
 * function.
 *
 * @return (bool) True
 */
function extras_enabled() {
    return true;
}
