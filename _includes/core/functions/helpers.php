<?php

/**
 * Get current year for
 * copyright notice
 *
 * @since 1.0.0
 */
function this_year() {
    return BP\Filters::apply('this_year', date('Y'));
}
