<?php

/*****************************
 * @NOTE Global functions will
 * be deprecated in a future release
 * Boilerplate in favour of a more
 * Object oriented approach.
 *
 * I recommend dependency injection, e.g.
 *
 * use BP\Filters;
 * class MyClass {
 *
 *      private $filters;
 *      public function __construct( Filters $filters ) {
 *          $this->filters = $filters;
 *      }
 *
 * }
 *
 *
 * For theme use, or if you prefer,
 * there are static methods available:
 *
 * use BP\Filters;
 * Filters::add('hook', 'callback'); <-- Same as add_filter()
 * Filters::apply('hook', 'value'); <-- Same as apply_filters()
 ****************************/

/**
 * Add Filter
 *
 * @since 1.0.0
 * @updated 1.5.4
 *
 * $hook: (string) name of the filter hook
 * $name: (string) name of the callback function
 * @deprecated $args: (int) number of arguments
 * $priority: (int) the execution priority (lower = run first)
 */
function add_filter( $hook, $name, $args = 1, $priority = 10 ) {
    $filters = BP\Filters::get_instance();
    $filters->add_filter( $hook, $name, $args, $priority );
}

/**
 * Apply Filters
 *
 * @since 1.0.0
 * @updated 1.5.4
 *
 * $hook: (string) name of the filter hook
 * $input: (mixed) value to filter
 */
function apply_filters( $hook, $input ) {
    $filters = BP\Filters::get_instance();
    return $filters->apply_filters( $hook, $input );
}
